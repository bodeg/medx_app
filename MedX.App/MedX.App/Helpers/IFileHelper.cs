﻿namespace MedX.App.Helpers
{
    public interface IFileHelper
    {
        string GetLocalFilePath(string filename);
    }
}
