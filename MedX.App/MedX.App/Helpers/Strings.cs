﻿namespace MedX.App.Helpers
{
    internal class Strings
    {
        public static string AppTitle { get; } = "MedX.App";
        public static string SystemMessageTitle { get; } = "System Message:";
        public static string AlertCloseButtonText { get; } = "Close";
        public static string BackButtonText { get; } = "Back";
        public static string NextButtonText { get; } = "Next";
        public static string FinishButtonText { get; } = "Done";
    }
}
