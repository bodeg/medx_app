﻿using Plugin.Settings;
using Plugin.Settings.Abstractions;

namespace MedX.App.Helpers
{
    // https://jamesmontemagno.github.io/SettingsPlugin/
    public static class Settings
    {
        public static ISettings AppSettings => CrossSettings.IsSupported ? CrossSettings.Current : null;
    }
}