﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Xamarin.Forms;

namespace MedX.App.XF
{
    public class VisualTreeHelper
    {
        // https://forums.xamarin.com/discussion/18599/visual-tree-helper

        public static List<T> FindVisualChildren<T>(
            /*this*/ VisualElement parentElement,
            VisualElement whereSearch,
            string containsStringName = null,
            List<T> result = null)
        {
            result = result ?? new List<T>();

            try
            {
                var props = whereSearch.GetType().GetRuntimeProperties().ToList();
                var contProp = props.FirstOrDefault(w => w.Name == "Content");
                var childProp = props.FirstOrDefault(w => w.Name == "Children");

                if (childProp == null)
                {
                    if (contProp != null && contProp.GetValue(whereSearch) is VisualElement cv)
                    {
                        FindVisualChildren<T>(parentElement, cv, containsStringName, result);
                    }

                    return result;
                }

                var v = childProp.GetValue(whereSearch) as IEnumerable;
                foreach (var i in v)
                {
                    if (i is VisualElement)
                        FindVisualChildren<T>(parentElement, i as VisualElement, containsStringName, result);

                    if (i is T ii)
                    {
                        if (!string.IsNullOrEmpty(containsStringName))
                        {
                            var fCheck = false;
                            var fields = parentElement.GetType().GetRuntimeFields()
                                .Where(w => w.Name.ToLower().Contains(containsStringName.ToLower())).ToList();
                            foreach (var f in fields)
                            {
                                var fv = f.GetValue(parentElement);
                                if (fv is T && fv == i)
                                {
                                    fCheck = true;
                                    break;
                                }
                            }

                            if (!fCheck) continue;
                        }

                        result.Add(ii);
                    }
                }

                return result;
            }
            catch
            {
                return result;
            }
        }
    }
}