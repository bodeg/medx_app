﻿using System;
using System.Globalization;

namespace MedX.App.XF
{
    public interface IMultiValueConverter
    {
        object Convert(object[] values, Type targetType, object parameter, CultureInfo culture);
    }
}