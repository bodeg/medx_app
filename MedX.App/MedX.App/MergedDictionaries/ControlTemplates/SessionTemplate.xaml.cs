﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MedX.App.MergedDictionaries.ControlTemplates
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SessionTemplate : ResourceDictionary
    {
        public SessionTemplate()
        {
            InitializeComponent();
        }
    }
}