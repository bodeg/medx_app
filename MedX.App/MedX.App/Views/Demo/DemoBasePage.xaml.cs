﻿using System.Windows.Input;
using Xamarin.Forms;

namespace MedX.App.Views.Demo
{
    public partial class DemoBasePage : ContentPage
    {
        public static readonly BindableProperty HeaderTextProperty =
            BindableProperty.Create("HeaderText", typeof(string), typeof(DemoPage), null);

        public static readonly BindableProperty FooterTextProperty =
            BindableProperty.Create("FooterText", typeof(string), typeof(DemoPage), null);

        public static readonly BindableProperty DemoCommandProperty =
            BindableProperty.Create(nameof(DemoCommand), typeof(ICommand), typeof(DemoPage), null);

        public string HeaderText => (string)GetValue(HeaderTextProperty);
        public string FooterText => (string)GetValue(FooterTextProperty);

        public ICommand DemoCommand
        {
            get => (ICommand)GetValue(DemoCommandProperty);
            set => SetValue(DemoCommandProperty, value);
        }
        public DemoBasePage()
        {
            InitializeComponent();
        }
    }
}
