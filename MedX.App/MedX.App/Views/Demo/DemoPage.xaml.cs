﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using MedX.App.XF;
using Syncfusion.SfNavigationDrawer.XForms;
using Syncfusion.SfRating.XForms;
using Xamarin.Forms;

namespace MedX.App.Views.Demo
{
    public partial class DemoPage : DemoBasePage
    {
        private Button HamburgerButton { get; }
        private List<string> list = new List<string>();

        public DemoPage()
        {
            InitializeComponent();

            //HamburgerButton = hamburgerButton;

            //Rating_InitializeItems();

            MainLayout.SizeChanged += (sender, args) =>
            {
                if (MainLayout.Width > 0)
                {
                    var itemSize = (int) ((MainLayout.Width - (Rating.ItemCount + 1) * Rating.ItemSpacing) /
                                          Rating.ItemCount);
                    Rating.ItemSize = Math.Min(32, itemSize);
                    Rating.Margin = new Thickness(Rating.ItemSpacing, 0);
                }
            };

            if (HamburgerButton != null)
            {
                navigationDrawer.DrawerWidth = 200;
                HamburgerButton.Image = (FileImageSource) ImageSource.FromFile("symbol_bug.png");
            }

            //navigationDrawer.DrawerHeaderHeight = 0f;
            //navigationDrawer.DrawerFooterHeight = 0f;

            list.Add("Home");
            list.Add("Profile");
            list.Add("Inbox");
            list.Add("Out box");
            list.Add("Sent");
            list.Add("Draft");
            listView.ItemsSource = list;
        }

        private void Rating_InitializeItems()
        {
            var ratingItems = new ObservableCollection<SfRatingItem>();

            for (var i = 0; i < Rating.ItemCount; i++)
            {
                var img = i <= Rating.Value
                    ? $"number_{(i + 1) % 10}_filled.png"
                    : $"number_{(i + 1) % 10}.png";

                var item = new SfRatingItem
                {
                    SelectedView = new Image() {Source = img, Aspect = Aspect.Fill},
                    UnSelectedView = new Image() {Source = img, Aspect = Aspect.Fill}
                };

                ratingItems.Add(item);
            }

            Rating.Items = ratingItems;
            Rating.EnableCustomView = true;
        }

        void hamburgerButton_Clicked(object sender, EventArgs e)
        {
            navigationDrawer.ToggleDrawer();
        }

        private void listView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            // Your codes here
            // navigationDrawer.ToggleDrawer();
        }

        private void ToolbarItem_Clicked(object sender, EventArgs e)
        {
            var aNavigationDrawerList = VisualTreeHelper.FindVisualChildren<SfNavigationDrawer>(this, this);
            if (aNavigationDrawerList.Any())
            {
                //aNavigationDrawerList = VisualTreeHelper.FindVisualChildren<SfNavigationDrawer>(aNavigationDrawerList[0].ContentView, aNavigationDrawerList[0].ContentView);

                if (aNavigationDrawerList.Any())
                {
                    aNavigationDrawerList[0].ToggleDrawer();
                }

                return;
            }

            navigationDrawer.ToggleDrawer();
        }
    }
}