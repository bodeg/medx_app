﻿using MedX.App.ViewModels.Session;
using Xamarin.Forms;

namespace MedX.App.Views.Session
{
    public partial class WorkoutPage : BaseSessionPage
    {
        public WorkoutPage()
        {
            InitializeComponent();

            if (BindingContext is WorkoutViewModel vm)
            {
                WorkoutImage.Source = vm.ImageSource;

                // avoid flickering when change Image.Source
                // Bug_36600 - Changing an Image.Source causes a visual flickering due to height changes in layout
                // https://bugzilla.xamarin.com/show_bug.cgi?id=36600

                vm.PropertyChanged += (sender, args) =>
                {
                    if (args.PropertyName.Equals(nameof(vm.ImageSource)))
                    {
                        WorkoutImage.BatchBegin();
                        //await WorkoutImage.FadeTo(0, 250);

                        WorkoutImage.Source = vm.ImageSource;

                        WorkoutImage.BatchCommit();
                        //await WorkoutImage.FadeTo(1, 250);
                    }
                };
            }

            CircularGauge.WidthRequest = CircularGauge.HeightRequest = this.Width;
        }
    }
}
