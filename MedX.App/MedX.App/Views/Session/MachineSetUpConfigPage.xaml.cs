﻿using System;
using System.Threading.Tasks;
using MedX.App.ViewModels.Session;
using Syncfusion.ListView.XForms;
using Xamarin.Forms;

namespace MedX.App.Views.Session
{
    public partial class MachineSetUpConfigPage : ContentPage
    {
        private MachineSetUpConfigViewModel ViewModel => BindingContext as MachineSetUpConfigViewModel;

        public MachineSetUpConfigPage()
        {
            InitializeComponent();

            ListView.SwipeStarted += (sender, args) => _itemIndex = null;
            ListView.SwipeEnded += (sender, args) => _itemIndex = args.ItemIndex;

            ListView.ItemDragging += async (sender, args) =>
            {
                if (args.Action == DragAction.Drop)
                {
                    await Task.Delay(200);
                    ViewModel?.Items.Move(args.OldIndex, args.NewIndex);
                }
            };

            // https://github.com/jamesmontemagno/FloatingActionButton-for-Xamarin.Android
            FloatingActionButtonAdd.Clicked += (arg1, arg2) => ViewModel?.AddItemCommand.Execute();
        }


        private Image _leftImage;
        private Image _rightImage;
        private int? _itemIndex;

        private void EditItem()
        {
            ViewModel?.EditItemCommand.Execute(_itemIndex);
            ListView.ResetSwipe();
        }

        private void DeleteItem()
        {
            ViewModel?.DeleteItemCommand.Execute(_itemIndex);
            ListView.ResetSwipe();
        }

        private void LeftImage_BindingContextChanged(object sender, EventArgs e)
        {
            if (_leftImage != null) return;

            _leftImage = sender as Image;

            //_leftImage.Source = ImageSource.FromResource("Swiping.Images.Favorites.png");

            (_leftImage?.Parent as View)?.GestureRecognizers.Add(
                new TapGestureRecognizer() {Command = new Command(EditItem)});
        }

        private void RightImage_BindingContextChanged(object sender, EventArgs e)
        {
            if (_rightImage != null) return;

            _rightImage = sender as Image;

            //_rightImage.Source = ImageSource.FromResource("Swiping.Images.DeleteItem.png");

            (_rightImage?.Parent as View)?.GestureRecognizers.Add(
                new TapGestureRecognizer() {Command = new Command(DeleteItem)});
        }
    }
}