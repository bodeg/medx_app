﻿using System.Collections.ObjectModel;
using System.Windows.Input;
using MedX.App.Models;
using MedX.App.Views.Base;
using Xamarin.Forms;

namespace MedX.App.Views.Session
{
    public partial class BaseSessionPage : BasePage
    {
        public static readonly BindableProperty BgImageProperty =
            BindableProperty.Create(nameof(BgImage), typeof(string), typeof(BaseSessionPage), "background.png");
        public string BgImage => (string)GetValue(BgImageProperty);

        public static readonly BindableProperty HeaderTextProperty =
            BindableProperty.Create(nameof(HeaderText), typeof(string), typeof(BaseSessionPage), null);
        public string HeaderText => (string)GetValue(HeaderTextProperty);

        public static readonly BindableProperty FlowItemCountProperty =
            BindableProperty.Create(nameof(FlowItemCount), typeof(int), typeof(BaseSessionPage), 0);
        public int FlowItemCount => (int)GetValue(FlowItemCountProperty);

        public static readonly BindableProperty FlowItemsProperty =
            BindableProperty.Create(nameof(FlowItems), typeof(ObservableCollection<FlowListItem>), typeof(BaseSessionPage), null);
        public ObservableCollection<FlowListItem> FlowItems => (ObservableCollection<FlowListItem>)GetValue(FlowItemsProperty);

        public static readonly BindableProperty InstructionsTextProperty =
            BindableProperty.Create(nameof(InstructionsText), typeof(string), typeof(BaseSessionPage), null);
        public string InstructionsText => (string)GetValue(InstructionsTextProperty);

        public static readonly BindableProperty BackButtonTextProperty =
            BindableProperty.Create(nameof(BackButtonText), typeof(string), typeof(BaseSessionPage), null);
        public string BackButtonText => (string)GetValue(BackButtonTextProperty);

        public static readonly BindableProperty NextButtonTextProperty =
            BindableProperty.Create(nameof(NextButtonText), typeof(string), typeof(BaseSessionPage), null);
        public string NextButtonText => (string)GetValue(NextButtonTextProperty);

        public static readonly BindableProperty PlaySoundCommandProperty =
            BindableProperty.Create(nameof(PlaySoundCommand), typeof(ICommand), typeof(BaseSessionPage), null);
        public ICommand PlaySoundCommand => (ICommand)GetValue(PlaySoundCommandProperty);

        public static readonly BindableProperty BackCommandProperty =
            BindableProperty.Create(nameof(BackCommand), typeof(ICommand), typeof(BaseSessionPage), null);
        public ICommand BackCommand
        {
            get => (ICommand)GetValue(BackCommandProperty);
            set => SetValue(BackCommandProperty, value);
        }

        public static readonly BindableProperty NextCommandProperty =
            BindableProperty.Create(nameof(NextCommand), typeof(ICommand), typeof(BaseSessionPage), null);
        public ICommand NextCommand
        {
            get => (ICommand)GetValue(NextCommandProperty);
            set => SetValue(NextCommandProperty, value);
        }

        public BaseSessionPage()
        {
            InitializeComponent();
        }
    }
}
