﻿using System.Collections.ObjectModel;
using Syncfusion.SfRangeSlider.XForms;
using Xamarin.Forms;

namespace MedX.App.Views.Session
{
    public partial class PainLevelSurveyPage : BaseSessionPage
    {
        public PainLevelSurveyPage()
        {
            InitializeComponent();

            //var customLabels = new ObservableCollection<Items>();
            //for (var i = 0; i <= 10; i++)
            //{
            //    customLabels.Add(new Items() {Label = "Min", Value = i});
            //}
            //Slider.ShowCustomLabel = true;
            //Slider.CustomLabels = customLabels;

            MainLayout.SizeChanged += (sender, args) =>
            {
                if (MainLayout.Height <= 0) return;
                if (FlowListView.FlowItemsSource == null) return;

                FlowListView.RowHeight = (int)(MainLayout.Height / FlowListView.FlowItemsSource.Count);
            };
        }
    }
}
