﻿using MedX.App.Views.Base;

namespace MedX.App.Views
{
    public partial class HomePage : BasePage
    {
        protected override bool IsAppRootPage => true;
        public HomePage()
        {
            InitializeComponent();
        }
    }
}
