﻿using MedX.App.Views.Base;

namespace MedX.App.Views
{
    public partial class LoginPage : BasePage
    {
        protected override bool IsAppRootPage => true;
        public LoginPage()
        {
            InitializeComponent();
        }
    }
}
