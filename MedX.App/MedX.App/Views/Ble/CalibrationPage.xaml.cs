﻿using System;
using System.Collections;
using System.Collections.ObjectModel;
using MedX.App.Models;
using MedX.App.ViewModels.Ble;
using Syncfusion.SfDataGrid.XForms;
using Syncfusion.SfPicker.XForms;
using Xamarin.Forms;

namespace MedX.App.Views.Ble
{
    public partial class CalibrationPage : BaseBlePage
    {
        private CalibrationViewModel ViewModel => BindingContext as CalibrationViewModel;

        public ObservableCollection<object> PickerItemsSource => _pickerItems.MainCollection;
        public object PickerSelectedItem => _pickerItems.SelectedItem;
        public ObservableCollection<string> PickerColumnHeaderText => _pickerItems.Header;

        private readonly PickerCascadingData _pickerItems = new PickerCascadingData();
        private RangeOfMotion _currentPickerItem;
        private double _selectedDataPointValue;

        public CalibrationPage()
        {
            InitializeComponent();

            Chart_InitializeComponent();
            Picker_InitializeComponent();
        }

        private void Chart_InitializeComponent()
        {
            //Chart.WidthRequest = this.Width;
            //Chart.HeightRequest = this.Width;

            Chart.SelectionChanged += (sender, args) =>
            {
                if (ViewModel == null) return;
                if (args.SelectedDataPointIndex < 0) return;
                if (args.SelectedDataPointIndex > ViewModel.DataPoints.Count) return;

                _selectedDataPointValue = ViewModel.DataPoints[args.SelectedDataPointIndex].YValue;
                Picker.IsOpen = true;
            };
        }

        private void Picker_InitializeComponent()
        {
            Picker.WidthRequest = Width * 0.9;
            Picker.HeightRequest = Height * 0.5;
            Picker.PickerWidth = 320;
            Picker.PickerHeight = 280;
            Picker.ShowColumnHeader = PickerColumnHeaderText != null;

            Picker.SelectionChanged += Picker_SelectionChanged;
            Picker.OkButtonClicked += Picker_OkButtonClicked;

            _pickerItems.Header.Add("Range of Motion");
            _pickerItems.Header.Add("Settings");

            _pickerItems.MasterCollection.Add(RangeOfMotion.Flexion);
            _pickerItems.MasterCollection.Add(RangeOfMotion.ExtLimiter);

            foreach (var item in GetDetailCollection(_pickerItems.MasterCollection[0]))
            {
                _pickerItems.DetailCollection.Add(item);
            }

            _pickerItems.SelectedItem =
                new ObservableCollection<object> {_pickerItems.MasterCollection[0], _pickerItems.DetailCollection[0]};
        }

        private void Picker_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!(e.NewValue is IList newValue)) return;

            if (Picker.ItemsSource != null && newValue.Count > 0 && _currentPickerItem != (RangeOfMotion) newValue[0])
            {
                //Updated the second column collection based on first column selected value.
                (Picker.ItemsSource as ObservableCollection<object>)?.RemoveAt(1);
                (Picker.ItemsSource as ObservableCollection<object>)?.Add(GetDetailCollection(newValue[0]));
            }
        }

        private void Picker_OkButtonClicked(object sender, SelectionChangedEventArgs e)
        {
            if (!(e.NewValue is ObservableCollection<object> newValue)) return;

            if (newValue.Count == 2)
            {                
                var setupItem = new InclinometerSetupItem
                {
                    Value = _selectedDataPointValue,
                    RangeOfMotion = (RangeOfMotion) newValue[0],
                    RomSettings = (int) newValue[1]
                };

                ViewModel?.SelectDataPointCommand.Execute(setupItem);
            }
        }

        public ObservableCollection<object> GetDetailCollection(object item)
        {
            _currentPickerItem = (RangeOfMotion) item;

            const int min = 1;
            var max = 0;

            if (_currentPickerItem == RangeOfMotion.Flexion)
            {
                max = 12;
            }
            else if (_currentPickerItem == RangeOfMotion.ExtLimiter)
            {
                max = 9;
            }

            var detailCollection = new ObservableCollection<object>();

            for (var i = min; i <= max; i++)
                detailCollection.Add(i);

            return detailCollection;
        }

        private Image _leftImage;
        private int _swipedRowIndex;

        private void DataGrid_SwipeEnded(object sender, SwipeEndedEventArgs e)
        {
            _swipedRowIndex = e.RowIndex;
        }

        private void LeftImage_BindingContextChanged(object sender, EventArgs e)
        {
            if (_leftImage != null) return;

            _leftImage = sender as Image;

            //_rightImage.Source = ImageSource.FromResource("Swiping.Images.DeleteItem.png");

            (_leftImage?.Parent as View)?.GestureRecognizers.Add(
                new TapGestureRecognizer() { Command = new Command(Delete) });
        }

        private void Delete()
        {
            ViewModel?.ClearRomSettingCommand.Execute(_swipedRowIndex - 1);
            DataGrid.ResetSwipeOffset();
        }
    }
}