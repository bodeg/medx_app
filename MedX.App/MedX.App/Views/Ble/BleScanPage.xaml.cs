﻿using MedX.App.BluetoothLE.Abstractions;
using MedX.App.ViewModels.Ble;
using MedX.App.Views.Base;

namespace MedX.App.Views.Ble
{
    public partial class BleScanPage : BaseBlePage
    {
        public BleScanPage()
        {
            InitializeComponent();

            if (BindingContext is BleScanViewModel vm)
            {
                PullToRefresh.Refreshing += (sender, args) => vm.StartCommand.Execute();

                ListView.ItemHolding += (sender, args) =>
                {
                    ListView.SelectedItem = args.ItemData;
                    vm.SetDefaultBleDeviceCommand.Execute(args.ItemData as IBleDevice);
                };
            }
        }
    }
}
