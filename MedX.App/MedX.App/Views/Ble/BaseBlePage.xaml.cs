﻿using MedX.App.Views.Base;

namespace MedX.App.Views.Ble
{
    public partial class BaseBlePage : BasePage
    {
        public BaseBlePage()
        {
            InitializeComponent();

            ToolbarItems.Remove(DebugToolbarItem);
        }
    }
}
