﻿using MedX.App.ViewModels.Ble;
using Xamarin.Forms;

namespace MedX.App.Views.Ble
{
    public partial class BleInfoPage : BaseBlePage
    {
        private BleInfoViewModel ViewModel => BindingContext as BleInfoViewModel;

        public BleInfoPage()
        {
            InitializeComponent();

            PullToRefresh.Refreshing += (sender, args) => ViewModel?.RefreshCommand.Execute();
        }
    }
}
