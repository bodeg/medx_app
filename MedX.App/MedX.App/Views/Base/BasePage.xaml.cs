﻿using System.Windows.Input;
using MedX.App.Helpers;
using Xamarin.Forms;

namespace MedX.App.Views.Base
{
    public partial class BasePage : ContentPage
    {
        public static readonly BindableProperty DebugCommandProperty =
            BindableProperty.Create(nameof(DebugCommand), typeof(ICommand), typeof(BasePage), null);
        public ICommand DebugCommand => (ICommand)GetValue(DebugCommandProperty);

        protected virtual bool IsAppRootPage => false;
        public BasePage()
        {
            InitializeComponent();

            DebugCommand.CanExecuteChanged += (sender, args) =>
                DebugToolbarItem.Icon = DebugCommand.CanExecute(null) ? "bug_green.png" : "bug_yellow.png";
        }

        protected override bool OnBackButtonPressed()
        {
            if (Navigation.NavigationStack.Count > 1)
            {
                return base.OnBackButtonPressed();
            }

            if (IsAppRootPage)
            {
                CloseApp();
            }

            return true;
        }

        private async void CloseApp()
        {
            // https://stackoverflow.com/questions/30274004/xamarin-close-android-application-on-back-button/30571114#30571114
            var result = await DisplayAlert(Strings.AppTitle,
                "Do you want to shut down the application?", accept: "Shut Down", cancel: "Cancel");

            if (result)
            {
                DependencyService.Get<IAndroidMethods>()?.CloseApp();
            }
        }
    }
}
