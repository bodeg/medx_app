﻿using Xamarin.Forms;

namespace MedX.App.CustomControls
{
    // Using Custom Controls in Xamarin.Forms on Android - By James Montemagno
    // https://blog.xamarin.com/using-custom-controls-in-xamarin-forms-on-android/
    // https://github.com/jamesmontemagno/Xamarin.Forms-Android-CustomProgressBar

    // What is a HoloCircularProgressBar
    // https://github.com/jamesmontemagno/MonoDroidToolkit/wiki/HoloCircularProgressBar

    public class CircularProgress : View
    {
        public static readonly BindableProperty IndeterminateProperty = BindableProperty.Create(
            nameof(Indeterminate),
            typeof(bool),
            typeof(CircularProgress),
            default(bool));

        public bool Indeterminate
        {
            get => (bool) GetValue(IndeterminateProperty);
            set => SetValue(IndeterminateProperty, value);
        }

        public static readonly BindableProperty ProgressProperty = BindableProperty.Create(
            nameof(Progress),
            typeof(float),
            typeof(CircularProgress),
            0f);

        /// <summary>
        /// Gets or sets the current progress
        /// </summary>
        /// <value>The progress.</value>
        public float Progress
        {
            get => (float) GetValue(ProgressProperty);
            set => SetValue(ProgressProperty, value);
        }

        public static readonly BindableProperty MaxProperty = BindableProperty.Create(
            nameof(Max),
            typeof(float),
            typeof(CircularProgress),
            100f);

        /// <summary>
        /// Gets or sets the max value
        /// </summary>
        /// <value>The max.</value>
        public float Max
        {
            get => (float) GetValue(MaxProperty);
            set => SetValue(MaxProperty, value);
        }


        public static readonly BindableProperty ProgressBackgroundColorProperty = BindableProperty.Create(
            nameof(ProgressBackgroundColor),
            typeof(Color),
            typeof(CircularProgress),
            Color.White);

        /// <summary>
        /// Gets or sets the ProgressBackgroundColorProperty
        /// </summary>
        /// <value>The color of the ProgressBackgroundColorProperty.</value>
        public Color ProgressBackgroundColor
        {
            get => (Color) GetValue(ProgressBackgroundColorProperty);
            set => SetValue(ProgressBackgroundColorProperty, value);
        }

        public static readonly BindableProperty ProgressColorProperty = BindableProperty.Create(
            nameof(ProgressColor),
            typeof(Color),
            typeof(CircularProgress),
            Color.Red);

        /// <summary>
        /// Gets or sets the progress color
        /// </summary>
        /// <value>The color of the progress.</value>
        public Color ProgressColor
        {
            get => (Color) GetValue(ProgressColorProperty);
            set => SetValue(ProgressColorProperty, value);
        }


        public static readonly BindableProperty IndeterminateSpeedProperty = BindableProperty.Create(
            nameof(IndeterminateSpeed),
            typeof(int),
            typeof(CircularProgress),
            100);

        public int IndeterminateSpeed
        {
            get => (int) GetValue(IndeterminateSpeedProperty);
            set => SetValue(IndeterminateSpeedProperty, value);
        }


        public static readonly BindableProperty CircleStrokeWidthProperty = BindableProperty.Create(
            nameof(CircleStrokeWidth),
            typeof(int),
            typeof(CircularProgress),
            10);

        public int CircleStrokeWidth
        {
            get => (int)GetValue(CircleStrokeWidthProperty);
            set => SetValue(CircleStrokeWidthProperty, value);
        }
    }
}