﻿using System;
using Xamarin.Forms;

namespace MedX.App.CustomControls
{
    // http://venkyxamarin.blogspot.co.il/2017/12/how-to-set-corner-radius-for-view.html#more
    public class RoundedCornerView : Grid
    {
        public static readonly BindableProperty FillColorProperty = BindableProperty.Create(
            nameof(FillColor),
            typeof(Color),
            typeof(RoundedCornerView),
            Color.White);
        public Color FillColor
        {
            get => (Color)GetValue(FillColorProperty);
            set => SetValue(FillColorProperty, value);
        }

        public static readonly BindableProperty RoundedCornerRadiusProperty = BindableProperty.Create(
            nameof(RoundedCornerRadius),
            typeof(double),
            typeof(RoundedCornerView),
            3.0);
        public double RoundedCornerRadius
        {
            get => (double)GetValue(RoundedCornerRadiusProperty);
            set => SetValue(RoundedCornerRadiusProperty, value);
        }

        public static readonly BindableProperty MakeCircleProperty = BindableProperty.Create(
            nameof(MakeCircle),
            typeof(bool),
            typeof(RoundedCornerView),
            false);
        public bool MakeCircle
        {
            get => (bool)GetValue(MakeCircleProperty);
            set => SetValue(MakeCircleProperty, value);
        }

        public static readonly BindableProperty BorderColorProperty = BindableProperty.Create(
            nameof(BorderColor),
            typeof(Color),
            typeof(RoundedCornerView),
            Color.Transparent);
        public Color BorderColor
        {
            get => (Color)GetValue(BorderColorProperty);
            set => SetValue(BorderColorProperty, value);
        }

        public static readonly BindableProperty BorderWidthProperty = BindableProperty.Create(
            nameof(BorderWidth),
            typeof(int),
            typeof(RoundedCornerView),
            1);
        public int BorderWidth
        {
            get => (int)GetValue(BorderWidthProperty);
            set => SetValue(BorderWidthProperty, value);
        }
    }
}
