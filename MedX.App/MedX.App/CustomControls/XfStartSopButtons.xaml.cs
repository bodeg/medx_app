﻿using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MedX.App.CustomControls
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class XfStartSopButtons : ContentView
    {
        public static readonly BindableProperty StartCommandProperty =
            BindableProperty.Create(nameof(StartCommand), typeof(ICommand), typeof(XfStartSopButtons), null);

        public static readonly BindableProperty StopCommandProperty =
            BindableProperty.Create(nameof(StopCommand), typeof(ICommand), typeof(XfStartSopButtons), null);

        public static readonly BindableProperty IsRunningProperty =
            BindableProperty.Create(nameof(IsRunning), typeof(bool), typeof(XfStartSopButtons), false,
                BindingMode.OneWayToSource);

        public ICommand StartCommand
        {
            get => (ICommand)GetValue(StartCommandProperty);
            set => SetValue(StartCommandProperty, value);
        }

        public ICommand StopCommand
        {
            get => (ICommand)GetValue(StopCommandProperty);
            set => SetValue(StopCommandProperty, value);
        }

        public bool IsRunning
        {
            get => (bool)GetValue(IsRunningProperty);
            set => SetValue(IsRunningProperty, value);
        }

        public XfStartSopButtons()
        {
            InitializeComponent();

            StartButton.Clicked += (sender, args) => StartCommand?.Execute(null);
            StopButton.Clicked += (sender, args) => StopCommand?.Execute(null);
        }

        #region Overrides of Element

        protected override void OnPropertyChanged(string propertyName = null)
        {
            base.OnPropertyChanged(propertyName);
            if (propertyName == null) return;

            if (propertyName.Equals(nameof(StartCommand)))
            {
                if (StartCommand != null)
                {
                    StartCommand.CanExecuteChanged += (sender, args) => StartButton.IsEnabled = StartCommand.CanExecute(null);
                }
            }
            else if (propertyName.Equals(nameof(StopCommand)))
            {
                if (StopCommand != null)
                {
                    StopCommand.CanExecuteChanged += (sender, args) => StopButton.IsEnabled = StopCommand.CanExecute(null);
                }
            }
        }

        #endregion
    }
}