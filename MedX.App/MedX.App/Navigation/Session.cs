﻿using System;
using MedX.App.Views.Session;

namespace MedX.App.Navigation
{
    internal static class Session
    {
        public static string PainLevelSurvey { get; } = nameof(PainLevelSurveyPage);
        public static string CurrGoal { get; } = nameof(CurrGoalPage);
        public static string ReasonToReduce { get; } = nameof(ReasonToReducePage);
        public static string PrevGoal { get; } = nameof(PrevGoalPage);
        public static string MachineSetUp { get; } = nameof(MachineSetUpPage);
        public static string CustomGoal { get; } = nameof(CustomGoalPage);
        public static string Workout { get; } = nameof(WorkoutPage);        
        public static string WorkoutSummary { get; } = nameof(WorkoutSummaryPage);
        public static string Scores { get; } = nameof(ScoresPage);
        public static string MachineSetUpConfig { get; } = nameof(MachineSetUpConfigPage);        
    }
}