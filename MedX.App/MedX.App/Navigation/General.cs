﻿using MedX.App.Views;

namespace MedX.App.Navigation
{
    internal static class General
    {
        public static string NavigatedFrom { get; } = "NavigatedFrom";
        public static string Login { get; } = nameof(LoginPage); 
        public static string Home { get; } = nameof(HomePage);
        public static string EntryPopup { get; } = nameof(EntryPopupView); 
    }
}