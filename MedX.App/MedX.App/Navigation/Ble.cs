﻿using MedX.App.Views.Ble;

namespace MedX.App.Navigation
{
    internal static class Ble
    {
        public static string BleInfo { get; } = nameof(BleInfoPage);
        public static string BleScan { get; } = nameof(BleScanPage);
        public static string Calibration { get; } = nameof(CalibrationPage);        
    }
}