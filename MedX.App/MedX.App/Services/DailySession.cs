﻿using System;
using MedX.App.Models;

namespace MedX.App.Services
{
    public class DailySession
    {
        private static volatile Session _instance;
        private static readonly object SyncRoot = new object();

        private DailySession() { }

        public static Session Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (SyncRoot)
                    {
                        if (_instance == null)
                        {
                            _instance = new Session
                            {
                                Id = 0,
                                Time = DateTime.Now + TimeSpan.FromMinutes(5),
                                Type = SessionType.Flex,
                                Status = SessionStatus.NotStarted,
                                
                                CurrGoal = new Goal
                                {
                                    ExtensionLimiter = 9,
                                    FlexionPosition = 12,
                                    PistonColor = PistonColor.Red,
                                    PistonSettings = 4,
                                    Repetitions = 3
                                },

                                PrevGoal = new Goal
                                {
                                    ExtensionLimiter = 4,
                                    FlexionPosition = 8,
                                    PistonColor = PistonColor.Blue,
                                    PistonSettings = 3,
                                    Repetitions = 3
                                }
                            };

                            _instance.ResetValues();
                        }
                    }
                }

                return _instance;
            }
        }
    }
}
