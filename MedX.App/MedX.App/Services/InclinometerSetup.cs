﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using MedX.App.Helpers;
using MedX.App.Models;
using Newtonsoft.Json;

namespace MedX.App.Services
{
    public class InclinometerSetup
    {
        private static InclinometerSetup _instance;

        private InclinometerSetup()
        {
        }

        public static InclinometerSetup Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new InclinometerSetup();
                    _instance.GetValueOrDefault();
                }

                return _instance;
            }
        }

        public ObservableCollection<InclinometerSetupItem> Items { get; } =
            new ObservableCollection<InclinometerSetupItem>();

        private string AppSettingsKey => nameof(InclinometerSetup);

        public void GetValueOrDefault()
        {
            var jsonString = Settings.AppSettings.GetValueOrDefault(AppSettingsKey, string.Empty);

            if (string.IsNullOrEmpty(jsonString))
            {
                GetDefault();
            }
            else
            {
                try
                {
                    var items = JsonConvert.DeserializeObject<ObservableCollection<InclinometerSetupItem>>(jsonString);

                    Items.Clear();
                    foreach (var item in items)
                    {
                        Items.Add(item);
                    }
                }
                catch (Exception exception)
                {
                    Debug.WriteLine(exception.Message);
                    GetDefault();
                }
            }
        }

        public void GetDefault()
        {
            Items.Clear();

            for (var i = 1; i <= 12; i++)
            {
                Items.Add(new InclinometerSetupItem
                {
                    RangeOfMotion = RangeOfMotion.Flexion,
                    RomSettings = i
                });
            }

            for (var i = 1; i <= 9; i++)
            {
                Items.Add(new InclinometerSetupItem
                {
                    RangeOfMotion = RangeOfMotion.ExtLimiter,
                    RomSettings = i
                });
            }
        }

        public void UpdateValue()
        {
            var jsonString = JsonConvert.SerializeObject(Items);
            Debug.WriteLine(jsonString);

            Settings.AppSettings.AddOrUpdateValue(AppSettingsKey, jsonString);
        }
    }
}