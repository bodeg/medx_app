﻿using System;
using Newtonsoft.Json;

namespace MedX.App.Services
{
    public class BleSetup : BaseSetup
    {
        private static volatile BleSetup _instance;
        private static readonly object SyncRoot = new object();

        public static BleSetup Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (SyncRoot)
                    {
                        if (_instance == null)
                        {
                            _instance = new BleSetup();
                            _instance.GetValueOrDefault();
                        }
                    }
                }

                return _instance;
            }
        }

        private BleSetup() { }

        private Guid _bleDeviceId = Guid.Empty;

        public Guid BleDeviceId
        {
            get => _bleDeviceId;
            set => SetProperty(ref _bleDeviceId, value);
        }

        protected override string AppSettingsKey => $"{nameof(BleSetup)}";

        protected override void DeserializeJsonString(string jsonString)
        {
            _bleDeviceId = JsonConvert.DeserializeObject<Guid>(jsonString);
        }

        protected override string SerializeToJsonString()
        {
            return JsonConvert.SerializeObject(_bleDeviceId);
        }

        public override void GetDefault()
        {
            _bleDeviceId = Guid.Empty;
        }
    }
}