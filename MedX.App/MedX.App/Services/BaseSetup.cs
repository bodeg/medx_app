﻿using System;
using System.Diagnostics;
using MedX.App.Helpers;
using Prism.Mvvm;

namespace MedX.App.Services
{
    public abstract class BaseSetup : BindableBase
    {
        protected abstract string AppSettingsKey { get; }
        protected abstract void DeserializeJsonString(string jsonString);
        protected abstract string SerializeToJsonString();

        public virtual void GetValueOrDefault()
        {
            var jsonString = Settings.AppSettings.GetValueOrDefault(AppSettingsKey, string.Empty);

            if (string.IsNullOrEmpty(jsonString))
            {
                GetDefault();
            }
            else
            {
                try
                {
                    DeserializeJsonString(jsonString);
                }
                catch (Exception exception)
                {
                    Debug.WriteLine($"{this.GetType().Name}: {exception.Message}");
                    GetDefault();
                }
            }
        }

        public abstract void GetDefault();

        public void UpdateValue()
        {
            var jsonString = SerializeToJsonString();
            Debug.WriteLine(jsonString);

            Settings.AppSettings.AddOrUpdateValue(AppSettingsKey, jsonString);
        }
    }
}