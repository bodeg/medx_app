﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using MedX.App.Helpers;
using MedX.App.Models;
using Newtonsoft.Json;

namespace MedX.App.Services
{
    public class MachineSetup
    {
        private static readonly Dictionary<SessionType?, MachineSetup> Instances = new Dictionary<SessionType?, MachineSetup>();

        private MachineSetup()
        {
        }

        public static MachineSetup Instance(SessionType? sessionType)
        {
            if (!Instances.ContainsKey(sessionType))
            {
                var machineSetup = new MachineSetup(sessionType);
                machineSetup.GetValueOrDefault();
                    
                Instances.Add(sessionType, machineSetup);
            }

            return Instances[sessionType];
        }

        public SessionType? SessionType { get; }
        public ObservableCollection<MachineSetupItem> Steps { get; } = new ObservableCollection<MachineSetupItem>();

        public MachineSetup(SessionType? sessionType)
        {
            SessionType = sessionType;
        }

        private string AppSettingsKey => $"{nameof(MachineSetup)}.{SessionType}";
        public void GetValueOrDefault()
        {
            var jsonString = Settings.AppSettings.GetValueOrDefault(AppSettingsKey, string.Empty);

            if (string.IsNullOrEmpty(jsonString))
            {
                GetDefault();
            }
            else
            {
                try
                {
                    var steps = JsonConvert.DeserializeObject<ObservableCollection<MachineSetupItem>>(jsonString);

                    Steps.Clear();
                    foreach (var step in steps)
                    {
                        Steps.Add(step);
                    }
                }
                catch (Exception exception)
                {
                    Debug.WriteLine(exception.Message);
                    GetDefault();
                }
            }
        }

        public void GetDefault()
        {
            string[] steps;

            switch (SessionType)
            {
                case Models.SessionType.Flex:
                    steps = new[]
                    {
                        @"Attach the #piston.color# RESISTANCE PISTON on the machine.",
                        @"Adjust the RESISTANCE PISTON settings to #piston.settings#.",
                        @"Set the Extension Range Of Motion Limiter to #rom.limiter#.",
                        @"Using the restraint adjustment, properly secure yourself in the machine.",
                        @"Once properly restrained in the machine, use right hand to set Flex Range Of Motion (ROM) setting to position #rom.flex#.",
                        @"Squeeze on what looks like a bicycle break on the upper right hand side of the handle bar and slowly bend forward.",
                        @"If you cannot move to this position contact your Personal Clinic or Telephonic Clinician.",
                        @"Slowly lean backwards against the upper back pad until you feel the machine rest against the stopper and hold this position for xx seconds.",
                        @"After holding for the designated time, move to an upright position and rest for xx seconds.",
                    };
                    break;

                case Models.SessionType.Endurance:
                    steps = new[]
                    {
                        @"Attach the #piston.color# RESISTANCE PISTON on the machine.",
                        @"Adjust the RESISTANCE PISTON settings to #piston.settings#.",
                        @"Set the Extension Range Of Motion Limiter to #rom.limiter#.",
                        @"Using the restraint adjustment, properly secure yourself in the machine.",
                        @"Once properly restrained in the machine, use right hand to set Flex Range Of Motion (ROM) setting to position #rom.flex#.",
                        @"Squeeze on what looks like a bicycle break on the upper right hand side of the handle bar and slowly bend forward.",
                        @"If you cannot move to this position contact your Personal Clinic or Telephonic Clinician.",
                        @"Push back slowly against upper back pad and extend backwards into extensions as far as comfortable or until you feel a stopper. This should take 3-4 seconds.",
                        @"Pause for 1-2 seconds in the most extended position and then slowly lower the resistance for 3-4 seconds to starting flexed position.",
                        @"Keep a loose grip on the handlebars and do not hold your breath while exercising."
                    };
                    break;

                case null:
                default:
                    throw new ArgumentOutOfRangeException();
            }

            Steps.Clear();
            foreach (var stepText in steps)
            {
                Steps.Add(new MachineSetupItem { Text = stepText });
            }
        }

        public void UpdateValue()
        {
            var jsonString = JsonConvert.SerializeObject(Steps);
            Debug.WriteLine(jsonString);

            Settings.AppSettings.AddOrUpdateValue(AppSettingsKey, jsonString);
        }
    }
}