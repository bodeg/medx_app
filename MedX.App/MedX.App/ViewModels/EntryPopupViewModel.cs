﻿using Prism.Commands;
using MedX.App.ViewModels.Base;
using Prism.Navigation;
using Prism.Services;

namespace MedX.App.ViewModels
{
	public class EntryPopupViewModel : ViewModelBase
    {
        private string _text;
        private string _cancelButtonText;
        private string _acceptButtonText;

        public string Text
        {
            get => _text;
            set => SetProperty(ref _text, value);
        }

        public string AcceptButtonText
        {
            get => _acceptButtonText;
            set => SetProperty(ref _acceptButtonText, value);
        }

        public string CancelButtonText
        {
            get => _cancelButtonText;
            set => SetProperty(ref _cancelButtonText, value);
        }

        public DelegateCommand AcceptCommand { get; }
        public DelegateCommand CancelCommand { get; }

        private object UserParam { get; set; }

        public EntryPopupViewModel(INavigationService navigationService, IPageDialogService pageDialogService)
            : base(navigationService, pageDialogService)
        {
            AcceptCommand = new DelegateCommand(ExecuteAcceptCommand);
            CancelCommand = new DelegateCommand(ExecuteCancelCommand);
        }

        public struct Parameters
        {
            public static string Title { get; } = $"{nameof(EntryPopupViewModel)}.{nameof(Title)}";
            public static string SubTitle { get; } = $"{nameof(EntryPopupViewModel)}.{nameof(SubTitle)}";
            public static string Text { get; } = $"{nameof(EntryPopupViewModel)}.{nameof(Text)}";
            public static string AcceptButtonText { get; } = $"{nameof(EntryPopupViewModel)}.{nameof(AcceptButtonText)}";
            public static string CancelButtonText { get; } = $"{nameof(EntryPopupViewModel)}.{nameof(CancelButtonText)}";
            public static string UserParam { get; } = $"{nameof(EntryPopupViewModel)}.{nameof(UserParam)}";
        }

        public override void OnNavigatingTo(NavigationParameters parameters)
	    {
	        base.OnNavigatingTo(parameters);

	        Title = parameters.GetValue<string>(Parameters.Title);
	        SubTitle = parameters.GetValue<string>(Parameters.SubTitle);
            Text = parameters.GetValue<string>(Parameters.Text);
            AcceptButtonText = parameters.GetValue<string>(Parameters.AcceptButtonText);
	        CancelButtonText = parameters.GetValue<string>(Parameters.CancelButtonText);
	        UserParam = parameters.GetValue<object>(Parameters.UserParam);
        }

	    private async void ExecuteAcceptCommand()
	    {
	        await NavigationService.GoBackAsync(new NavigationParameters
	        {
	            {Parameters.Text, Text},
	            {Parameters.UserParam, UserParam }
	        });
	    }

        private async void ExecuteCancelCommand()
        {
            await NavigationService.GoBackAsync();
        }

    }
}
