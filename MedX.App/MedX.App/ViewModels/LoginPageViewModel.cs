﻿using Prism.Commands;
using System;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;
using MedX.App.Helpers;
using MedX.App.Validations;
using MedX.App.ViewModels.Base;
using Prism.Navigation;
using Prism.Services;

namespace MedX.App.ViewModels
{
    public class LoginPageViewModel : ViewModelBase
    {
        private ValidatableObject<string> _userName = new ValidatableObject<string>();
        private ValidatableObject<string> _password = new ValidatableObject<string>();
        private ValidatableObject<bool> _isNotFailed = new ValidatableObject<bool>();

        public static string MockUserName { get; } = "alon";
        public static string MockPassword { get; } = "1234";

        public ValidatableObject<string> UserName
        {
            get => _userName;
            set => SetProperty(ref _userName, value);
        }

        public ValidatableObject<string> Password
        {
            get => _password;
            set => SetProperty(ref _password, value);
        }

        public ValidatableObject<bool> IsNotFailed
        {
            get => _isNotFailed;
            set => SetProperty(ref _isNotFailed, value);
        }

        public DelegateCommand LoginCommand { get; }
        public DelegateCommand ValidateUserNameCommand { get; }
        public DelegateCommand ValidatePasswordCommand { get; }

        public LoginPageViewModel(INavigationService navigationService, IPageDialogService pageDialogService)
            : base(navigationService, pageDialogService)
        {
            try
            {
                UserName.Value = Settings.AppSettings.GetValueOrDefault(nameof(UserName), string.Empty);
            }
            catch
            {
                // ignored
            }

            LoginCommand = new DelegateCommand(Login);
            ValidateUserNameCommand = new DelegateCommand(() => ValidateUserName());
            ValidatePasswordCommand = new DelegateCommand(() => ValidatePassword());

            AddValidations();
        }

        private async void Login()
        {
            IsBusy = true;
            IsNotFailed.Value = true;

            var isValid = Validate();
            var isAuthenticated = false;

            if (isValid)
            {
                try
                {
                    await Task.Delay(1000);

                    isAuthenticated = string.Equals(UserName.Value, MockUserName)
                                      && string.Equals(Password.Value, MockPassword);
                    IsNotFailed.Value = isAuthenticated;
                }
                catch (Exception ex)
                {
                    Debug.WriteLine($"[Login] error: {ex}");
                }
            }

            IsNotFailed.Validate();

            if (isAuthenticated)
            {
                try
                {
                    Settings.AppSettings.AddOrUpdateValue(nameof(UserName), UserName.Value);
                }
                catch
                {
                    // ignored
                }

                await NavigationService.NavigateAsync($"/NavigationPage/{Navigation.General.Home}");
            }

            IsBusy = false;
        }

        private void AddValidations()
        {
            _userName.Validations.Add(
                new IsNotNullOrEmptyRule<string> {ValidationMessage = "A user name is required."});
            _password.Validations.Add(new IsNotNullOrEmptyRule<string> {ValidationMessage = "A password is required."});
            _isNotFailed.Validations.Add(new IsNotFailedRule {ValidationMessage = "Invalid user name or password."});
        }

        private bool Validate()
        {
            var isValidUser = ValidateUserName();
            var isValidPassword = ValidatePassword();

            return isValidUser && isValidPassword;
        }

        private bool ValidateUserName()
        {
            _isNotFailed.Reset();
            return _userName.Validate();
        }

        private bool ValidatePassword()
        {
            _isNotFailed.Reset();
            return _password.Validate();
        }

        #region DebugCommand

        protected override bool CanExecuteDebugCommand() => true;

        protected override async void ExecuteDebugCommand()
        {
            var message = new StringBuilder();
            message.AppendLine($"UserName: {MockUserName}");
            message.AppendLine($"Password: {MockPassword}");
            message.AppendLine();
            message.AppendLine("Try wrong/empty username/password.");

            await PageDialogService.DisplayAlertAsync(string.Empty, message.ToString(), "Close");
        }

        public override void OnNavigatedTo(NavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);

            ShowDebugNotification("Meet Green-Bug, your private assistant.", true);
        }

        #endregion
    }
}