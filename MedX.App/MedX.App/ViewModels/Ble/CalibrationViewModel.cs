﻿using System;
using System.Collections.ObjectModel;
using System.Threading;
using System.Threading.Tasks;
using MedX.App.BluetoothLE.Common;
using MedX.App.Models;
using Prism.Commands;
using Prism.Navigation;
using Prism.Services;
using Syncfusion.SfChart.XForms;

namespace MedX.App.ViewModels.Ble
{
    public class CalibrationViewModel : BaseBleViewModel
    {
        public ObservableCollection<ChartDataPoint> DataPoints { get; } = new ObservableCollection<ChartDataPoint>();

        public ObservableCollection<InclinometerSetupItem> SetupItemCollection =>
            Services.InclinometerSetup.Instance.Items;

        private double _startValue = (Math.PI / 2) * 180.0 / Math.PI;
        private double _endValue = (-1 * Math.PI / 2) * 180.0 / Math.PI;
        private bool _isRunning;
        private int _xValue;
        private CancellationTokenSource _cancellationTokenSource;

        public double StartValue
        {
            get => _startValue;
            set => SetProperty(ref _startValue, value);
        }

        public double EndValue
        {
            get => _endValue;
            set => SetProperty(ref _endValue, value);
        }

        public bool IsRunning
        {
            set
            {
                if (SetProperty(ref _isRunning, value))
                {
                    StartCommand.RaiseCanExecuteChanged();
                    StopCommand.RaiseCanExecuteChanged();
                }
            }

            get => _isRunning;
        }

        public DelegateCommand StartCommand { get; }
        public DelegateCommand StopCommand { get; }

        public DelegateCommand<InclinometerSetupItem> SelectDataPointCommand { get; }
        public DelegateCommand<int?> ClearRomSettingCommand { get; }
        
        public CalibrationViewModel(INavigationService navigationService, IPageDialogService pageDialogService)
            : base(navigationService, pageDialogService)
        {
            StartCommand = new DelegateCommand(StartReading, () => !IsRunning);
            StopCommand = new DelegateCommand(StopReading, () => IsRunning);

            SelectDataPointCommand = new DelegateCommand<InclinometerSetupItem>(SelectDataPoint);
            ClearRomSettingCommand = new DelegateCommand<int?>(ClearRomSetting);
        }

        private async void StartReading()
        {
            var bleReader = new BleReader();
            bleReader.ReadValue += (sender, args) =>
            {
                if (args.Inclinometer == null) return;
                //Debug.WriteLine($"Inclinometer: {args.Inclinometer.Value}");

                while (DataPoints.Count > 50)
                {
                    DataPoints.RemoveAt(0);
                }

                DataPoints.Add(new ChartDataPoint(_xValue++, args.Inclinometer.Value));
            };

            try
            {
                IsRunning = true;
                using (_cancellationTokenSource = new CancellationTokenSource())
                {
                    await bleReader.Run(_cancellationTokenSource.Token);
                }
            }
            catch (TaskCanceledException)
            {
            }
            catch (Exception exception)
            {
                await PageDialogService.DisplayAlertAsync("ERROR", exception.Message, "Close");
            }
            finally
            {
                _cancellationTokenSource = null;
                IsRunning = false;
            }
        }

        private async void StopReading()
        {
            try
            {
                _cancellationTokenSource?.Cancel();
            }
            catch (ObjectDisposedException)
            {
            }
            catch (Exception exception)
            {
                await PageDialogService.DisplayAlertAsync("ERROR", exception.Message, "Close");
            }
        }

        private void SelectDataPoint(InclinometerSetupItem setupItem)
        {
            foreach (var item in SetupItemCollection)
            {
                if (item.RomSettings != setupItem.RomSettings) continue;

                item.Value = Convert.ToInt32(setupItem.Value);
                Services.InclinometerSetup.Instance.UpdateValue();
                break;
            }
        }

        private void ClearRomSetting(int? settingIndex)
        {
            if (settingIndex == null) return;
            if (settingIndex.Value < 0 || settingIndex.Value > SetupItemCollection.Count) return;

            SetupItemCollection[settingIndex.Value].Value = null;
            Services.InclinometerSetup.Instance.UpdateValue();
        }

        public override void OnNavigatedFrom(NavigationParameters parameters)
        {
            Services.InclinometerSetup.Instance.UpdateValue();
            base.OnNavigatedFrom(parameters);
        }
    }
}