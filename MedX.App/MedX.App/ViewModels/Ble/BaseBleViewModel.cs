﻿using System;
using MedX.App.BluetoothLE.Abstractions;
using MedX.App.BluetoothLE.Vanilla;
using MedX.App.Services;
using MedX.App.ViewModels.Base;
using Prism.Commands;
using Prism.Navigation;
using Prism.Services;

namespace MedX.App.ViewModels.Ble
{
    public class BaseBleViewModel : BasePageViewModel
    {
        public IBleAdapter Ble => BleAdapter.Instance;
        public Guid BleDeviceId => BleSetup.Instance.BleDeviceId;

        public DelegateCommand ScanCommand { get; }
        public DelegateCommand InfoCommand { get; }
        public DelegateCommand InclinometerCommand { get; }
        public DelegateCommand StrengthCommand { get; }

        public BaseBleViewModel(INavigationService navigationService, IPageDialogService pageDialogService)
            : base(navigationService, pageDialogService)
        {
            ScanCommand = new DelegateCommand(async () =>
                await NavigationService.NavigateAsync($"../{Navigation.Ble.BleScan}"));

            InfoCommand = new DelegateCommand(async () =>
                await NavigationService.NavigateAsync($"../{Navigation.Ble.BleInfo}"));

            InclinometerCommand = new DelegateCommand(async () =>
                await NavigationService.NavigateAsync($"../{Navigation.Ble.Calibration}"));

            StrengthCommand = new DelegateCommand(async () =>
                await NavigationService.NavigateAsync($"../{Navigation.Ble.Calibration}"));

            BleSetup.Instance.PropertyChanged += (sender, args) =>
            {
                if (args.PropertyName.Equals(nameof(BleSetup.BleDeviceId)))
                {
                    RaisePropertyChanged(nameof(BleDeviceId));
                }
            };
        }
    }
}