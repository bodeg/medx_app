﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Threading;
using MedX.App.BluetoothLE.Common;
using Prism.Commands;
using Prism.Navigation;
using Prism.Services;

namespace MedX.App.ViewModels.Ble
{
	public class BleInfoViewModel : BaseBleViewModel
    {
        private BleDeviceInfo _bleDeviceInfo = new BleDeviceInfo();
        private readonly BleInfo _bleInfo = new BleInfo();

        public BleDeviceInfo BleDeviceInfo
        {
            get => _bleDeviceInfo;
            set => SetProperty(ref _bleDeviceInfo, value);
        }

        public DelegateCommand RefreshCommand { get; }
        public DelegateCommand UpdateCommand { get; }

        public BleInfoViewModel(INavigationService navigationService, IPageDialogService pageDialogService)
	        : base(navigationService, pageDialogService)
	    {
	        RefreshCommand = new DelegateCommand(ExecuteRefreshCommand)
	            .ObservesCanExecute(() => IsNotBusy);

	        UpdateCommand = new DelegateCommand(ExecuteUpdateCommand)
	            .ObservesCanExecute(() => IsNotBusy);
        }

        #region Overrides of BindableBase

        protected override void OnPropertyChanged(PropertyChangedEventArgs args)
        {
            base.OnPropertyChanged(args);

            if (args.PropertyName.Equals(nameof(IsBusy)))
            {
                RefreshCommand.RaiseCanExecuteChanged();
            }
        }

        #endregion

        private async void ExecuteRefreshCommand()
        {
            try
            {
                IsBusy = true;
                using (var cancellationTokenSource = new CancellationTokenSource())
                {
                    BleDeviceInfo = await _bleInfo.ReadDeviceInfoAsync(cancellationTokenSource.Token);
                }
            }
            catch (Exception exception)
            {
                await PageDialogService.DisplayAlertAsync("ERROR", exception.Message, "Close");
            }
            finally
            {
                IsBusy = false;
            }
        }

        private async void ExecuteUpdateCommand()
        {
            var result = await PageDialogService.DisplayAlertAsync(
                "Confirm",
                message: "Update BLE device information",
                acceptButton: "OK",
                cancelButton: "Cancel");

            if (!result) return;

            try
            {
                IsBusy = true;

                using (var cancellationTokenSource = new CancellationTokenSource())
                {
                    await _bleInfo.WriteDeviceInfoAsync(BleDeviceInfo, cancellationTokenSource.Token);
                    BleDeviceInfo = await _bleInfo.ReadDeviceInfoAsync(cancellationTokenSource.Token);
                }
            }
            catch (Exception exception)
            {
                await PageDialogService.DisplayAlertAsync("ERROR", exception.Message, "Close");
            }
            finally
            {
                IsBusy = false;
            }
        }
        
        public override async void OnNavigatingTo(NavigationParameters parameters)
        {
            base.OnNavigatingTo(parameters);

            try
            {
                IsBusy = true;

                using (var cancellationTokenSource = new CancellationTokenSource())
                {
                    BleDeviceInfo = await _bleInfo.ReadDeviceInfoAsync(cancellationTokenSource.Token);
                }
            }
            catch (Exception exception)
            {
                Debug.WriteLine(exception.Message);
            }
            finally
            {
                IsBusy = false;
            }
        }

    }
}
