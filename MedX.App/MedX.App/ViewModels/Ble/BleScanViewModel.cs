﻿using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MedX.App.BluetoothLE.Abstractions;
using MedX.App.BluetoothLE.Common;
using MedX.App.Services;
using Prism.Commands;
using Prism.Navigation;
using Prism.Services;

namespace MedX.App.ViewModels.Ble
{
    public class BleScanViewModel : BaseBleViewModel
    {
        public DelegateCommand StartCommand { get; }
        public DelegateCommand StopCommand { get; }
        public DelegateCommand<IBleDevice> SetDefaultBleDeviceCommand { get; }

        public BleScanViewModel(INavigationService navigationService, IPageDialogService pageDialogService)
            : base(navigationService, pageDialogService)
        {
            StartCommand = new DelegateCommand(() => Ble.StartScanning());

            StopCommand = new DelegateCommand(() => Ble.StopScanning())
                .ObservesCanExecute(() => Ble.IsDiscovering);

            SetDefaultBleDeviceCommand = new DelegateCommand<IBleDevice>(SetDefaultBleDevice);
        }

        private async void SetDefaultBleDevice(IBleDevice bleDevice)
        {
            if (bleDevice == null) return;

            var title = !string.IsNullOrEmpty(bleDevice.Name) ? bleDevice.Name : @"(no name)";

            var result = await PageDialogService.DisplayAlertAsync(
                title,
                message: "Pair with the selected device", 
                acceptButton: "Pair", 
                cancelButton: "Cancel");

            if (!result) return;

            try
            {
                using (var tokenSource = new CancellationTokenSource())
                {
                    var bleInfo = new BleInfo {DeviceId = bleDevice.Id};
                    var deviceInfo = await bleInfo.ReadDeviceInfoAsync(tokenSource.Token);

                    var message = new StringBuilder();
                    message.AppendLine($"CompanyName: {deviceInfo.CompanyName}.");
                    message.AppendLine($"DeviceName: {deviceInfo.DeviceName}.");                    
                    message.AppendLine($"DeviceSN: {deviceInfo.DeviceSerialNo}.");
                    message.AppendLine($"ControlerVersion: {deviceInfo.ControlerVersion}.");
                    message.AppendLine($"FirmwareVersion: {deviceInfo.FirmwareVersion}.");
                    message.AppendLine($"ControlerSN: {deviceInfo.ControlerSerialNo}.");

                    await PageDialogService.DisplayAlertAsync(deviceInfo.BleName, message.ToString(), "Close");

                    BleSetup.Instance.BleDeviceId = bleDevice.Id;
                    BleSetup.Instance.UpdateValue();
                }
            }
            catch (Exception exception)
            {
                var message = new StringBuilder();
                message.AppendLine("Failed to connect.");
                message.AppendLine(exception.Message);

                await PageDialogService.DisplayAlertAsync(title, message.ToString(), "Close");
            }
        }
    }
}
