﻿using Prism.Commands;
using System;
using System.Diagnostics;
using System.Text;
using MedX.App.Helpers;
using MedX.App.Models;
using MedX.App.Services;
using MedX.App.ViewModels.Base;
using Prism.Navigation;
using Prism.Services;
using Xamarin.Forms;

namespace MedX.App.ViewModels
{
    public partial class HomePageViewModel : ViewModelBase
    {
        private double _startButtonOpacity = 1.0;

        public Models.Session Session { get; } = DailySession.Instance;

        public DelegateCommand StartSessionCommand { get; }
        public DelegateCommand PersonalAreaCommand { get; }
        public DelegateCommand ReportsCommand { get; }
        public DelegateCommand BleCommand { get; }
        

        public double StartButtonOpacity
        {
            get => _startButtonOpacity;
            set => SetProperty(ref _startButtonOpacity, value);
        }

        public string ImageSource
        {
            get
            {
                if (Session.Status == null)
                    return "home_blank.png";

                switch (Session.Status.Value)
                {
                    case SessionStatus.NotStarted:
                        return "home_clock_green.png";
                    case SessionStatus.Passed:
                        return "home_clock_red.png";
                    case SessionStatus.Completed:
                        return "home_exercise_completed.png";
                    case SessionStatus.Incomplete:
                        return "home_exercise_incomplete.png";
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        public string NotesText
        {
            get
            {
                if (Session.Status == SessionStatus.Completed)
                {
                    var instructions = new StringBuilder();
                    instructions.AppendLine("Today's session is completed:");
                    instructions.AppendLine("After a session it is best to ice your low back and perform stretches.");

                    return instructions.ToString();
                }

                return string.Empty;
            }
        }


        public HomePageViewModel(INavigationService navigationService, IPageDialogService pageDialogService)
            : base(navigationService, pageDialogService)
        {
            StartSessionCommand = new DelegateCommand(StartSession);
            PersonalAreaCommand = new DelegateCommand(PersonalArea);
            ReportsCommand = new DelegateCommand(Reports);

            BleCommand = new DelegateCommand(async () =>
            {
                var page = BleSetup.Instance.BleDeviceId == Guid.Empty
                    ? Navigation.Ble.BleScan
                    : Navigation.Ble.BleInfo;

                await NavigationService.NavigateAsync(page);
            });

            Session.PropertyChanged += Session_PropertyChanged;

            Device.StartTimer(TimeSpan.FromSeconds(10), TimerCallback);
        }

        private void Session_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs args)
        {
            if (args.PropertyName.Equals(nameof(Session.Status)))
            {
                RaisePropertyChanged(nameof(ImageSource));
                RaisePropertyChanged(nameof(NotesText));
            }
        }

        private bool TimerCallback()
        {
            Session.UpdateStatus();

            UpdateStartButtonOpacity();

            return true;
        }

        private void UpdateStartButtonOpacity()
        {
            var opacityTimeSpan = TimeSpan.FromMinutes(5);
            const double maxOpacity = 1.0;
            const double minOpacity = 0.1;

            switch (Session.Status)
            {
                case SessionStatus.NotStarted:
                    Debug.Assert(Session.Time >= DateTime.Now);
                    var timeLeft = Session.Time - DateTime.Now;
                    if (timeLeft > opacityTimeSpan)
                    {
                        StartButtonOpacity = maxOpacity;
                    }
                    else
                    {
                        var opacity =
                            timeLeft.TotalSeconds * (maxOpacity - minOpacity) / opacityTimeSpan.TotalSeconds +
                            minOpacity;
                        Debug.Assert(opacity >= minOpacity && opacity <= maxOpacity);
                        StartButtonOpacity = Math.Max(minOpacity, opacity);
                    }

                    break;
                case SessionStatus.Passed:
                    StartButtonOpacity = minOpacity;
                    break;
                case SessionStatus.Completed:
                case SessionStatus.Incomplete:
                    StartButtonOpacity = minOpacity;
                    break;
                case null:
                default:
                    StartButtonOpacity = maxOpacity;
                    break;
            }
        }

        private async void StartSession()
        {
            if (Session.Status != null && Session.Status.Value == SessionStatus.Completed)
            {
                var message = new StringBuilder();
                message.AppendLine("Performing more than one session a day is againt the protocol.");
                message.AppendLine("Please contact your personal clinician if you have a question or concern.");

                await PageDialogService.DisplayAlertAsync(Strings.SystemMessageTitle, message.ToString(), Strings.AlertCloseButtonText);
                return;
            }

            if (Session.Status != null && Session.Status.Value == SessionStatus.Passed)
            {
                var message = new StringBuilder();
                message.AppendLine("You missed a day...");

                await PageDialogService.DisplayAlertAsync(Strings.SystemMessageTitle, message.ToString(), @"Got It");
            }

            Session.ResetValues();

            await NavigationService.NavigateAsync(Navigation.Session.PainLevelSurvey);
        }

        protected void PersonalArea()
        {
            DisplayToBeDoneMessage(@"PERSONAL AREA");
        }

        protected void Reports()
        {
            DisplayToBeDoneMessage(@"REPORTS");
        }

        #region DebugCommand

        protected override bool CanExecuteDebugCommand() => true;

        protected override async void ExecuteDebugCommand()
        {
#pragma warning disable CS0618 // Type or member is obsolete
            await PageDialogService.DisplayActionSheetAsync(
                "Try more scenarios...",

                // NotStarted (5 mins)
                ActionSheetButton.CreateButton($"{SessionStatus.NotStarted} (5 mins)",
                    new DelegateCommand(() =>
                    {
                        Session.Status = null;
                        Session.Time = DateTime.Now + TimeSpan.FromMinutes(5);
                    })),

                // NotStarted (1 mins)
                ActionSheetButton.CreateButton($"{SessionStatus.NotStarted} (1 min)",
                    new DelegateCommand(() =>
                    {
                        Session.Status = null;
                        Session.Time = DateTime.Now + TimeSpan.FromMinutes(1);
                    })),

                // Passed
                ActionSheetButton.CreateButton($"{SessionStatus.Passed}",
                    new DelegateCommand(() =>
                    {
                        Session.Status = null;
                        Session.Time = DateTime.Now + TimeSpan.FromMinutes(-5);
                    })),

                // Completed
                ActionSheetButton.CreateButton($"{SessionStatus.Completed}",
                    new DelegateCommand(() => Session.Status = SessionStatus.Completed)),

                // Flex
                ActionSheetButton.CreateButton($"{SessionType.Flex}",
                    new DelegateCommand(() => Session.Type = SessionType.Flex)),

                // Endurance
                ActionSheetButton.CreateButton($"{SessionType.Endurance}",
                    new DelegateCommand(() => Session.Type = SessionType.Endurance)),
                ActionSheetButton.CreateButton("CLOSE", new DelegateCommand(() => { })));
#pragma warning restore CS0618 // Type or member is obsolete

            UpdateStartButtonOpacity();
            DoNotShowDebugNotifications(); 
        }

        public override void OnNavigatedTo(NavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);

            ShowDebugNotification("Use the Green-Bug for more scenarios.", false);
        }

        #endregion
    }
}