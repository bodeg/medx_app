﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using MedX.App.Models;
using Prism.Commands;
using Prism.Navigation;
using Prism.Services;
using Xamarin.Forms;

namespace MedX.App.ViewModels.Demo
{
    public class DemoPageViewModel : DemoBasePageViewModel
    {
        public int RatingItemCount { get; } = 3;
        public int RatingValue { get; } = 2;

        public ObservableCollection<FlowListItem> Items { get; } = new ObservableCollection<FlowListItem>();
        public DelegateCommand ClickCommand { get; }

        public DemoPageViewModel(INavigationService navigationService, IPageDialogService pageDialogService)
            : base(navigationService, pageDialogService)
        {
            HeaderText = $"{HeaderText} (0)";
            FooterText = $"{FooterText} (0)";

            ClickCommand = new DelegateCommand(ExecuteClick);

            for (var i = 0; i < RatingItemCount; i++)
            {
                var item = new FlowListItem
                {
                    Value = $"{i + 1}",
                    BackgroundColor = i % 2 == 0 ? Color.Red : Color.Green
                };

                Items.Add(item);
            }            
        }

        private async void ExecuteClick()
        {
            var result = await PageDialogService.DisplayAlertAsync("Click", "message-1", "accept", "cancel");
        }
    }
}

