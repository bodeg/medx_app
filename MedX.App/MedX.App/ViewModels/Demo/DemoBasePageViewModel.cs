﻿using MedX.App.ViewModels.Base;
using Prism.Commands;
using Prism.Navigation;
using Prism.Services;

// https://developer.xamarin.com/guides/xamarin-forms/application-fundamentals/templates/control-templates/

namespace MedX.App.ViewModels.Demo
{
    public class DemoBasePageViewModel : ViewModelBase
    {
        public string HeaderText { get; protected set; } = "I am HeaderText";

        public string FooterText { get; protected set; } = "I am FooterText";

        public string ButtonText => "Click Me!";

        public DelegateCommand DemoCommand { get; }

        public DemoBasePageViewModel(INavigationService navigationService, IPageDialogService pageDialogService)
            : base(navigationService, pageDialogService)
        {
            DemoCommand = new DelegateCommand(ExecuteDemo);
        }

        protected virtual async void ExecuteDemo()
        {
            var result = await PageDialogService.DisplayAlertAsync("title", "message", "accept", "cancel");
        }
    }
}
