﻿using MedX.App.ViewModels.Base;
using Prism.Commands;
using Prism.Navigation;
using Prism.Services;

namespace MedX.App.ViewModels.Demo
{
    public class DemoPopupViewModel : ViewModelBase
    {
        public DemoPopupViewModel(INavigationService navigationService, IPageDialogService pageDialogService)
            : base(navigationService, pageDialogService)
        {
            System.Diagnostics.Debug.WriteLine("Hello from the PopupViewViewModel");
            NavigateBackCommand = new DelegateCommand(OnNavigateBackCommandExecuted);
        }

        private string _message = "(message should be passed as a parameter)";

        public string Message
        {
            get => _message;
            set => SetProperty(ref _message, value);
        }

        public DelegateCommand NavigateBackCommand { get; }

        public override void OnNavigatedTo(NavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);

            if (parameters.ContainsKey("message"))
            {
                Message = parameters["message"].ToString();
            }
        }

        private async void OnNavigateBackCommandExecuted()
        {
            await NavigationService.GoBackAsync(new NavigationParameters
            {
                {"message", "Hello from the Popup View"}
            });
        }
    }
}
