﻿using Prism.Navigation;
using Prism.Services;

namespace MedX.App.ViewModels.Base
{
    public class BasePageViewModel : ViewModelBase
    {
        public BasePageViewModel(INavigationService navigationService, IPageDialogService pageDialogService)
            : base(navigationService, pageDialogService)
        {
        }
    }
}
