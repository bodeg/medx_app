﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Plugin.SimpleAudioPlayer.Abstractions;
using Plugin.Toasts;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;

namespace MedX.App.ViewModels.Base
{
    public class ViewModelBase : BindableBase, INavigationAware, IDestructible
    {
        protected static ISimpleAudioPlayer AudioPlayer = Plugin.SimpleAudioPlayer.CrossSimpleAudioPlayer.Current;

        protected INavigationService NavigationService { get; }
        protected IPageDialogService PageDialogService { get; }

        private string _title;
        private string _subTitle; 
        private bool _isBusy;
        private bool _isNotBusy = true;

        public string Title
        {
            get => _title;
            set => SetProperty(ref _title, value);
        }

        public string SubTitle
        {
            get => _subTitle;
            set => SetProperty(ref _subTitle, value);
        }
        
        public virtual bool IsBusy
        {
            get => _isBusy;
            set
            {
                if (SetProperty(ref _isBusy, value))
                    IsNotBusy = !_isBusy;
            }
        }

        public bool IsNotBusy
        {
            get => _isNotBusy;
            set
            {
                if (SetProperty(ref _isNotBusy, value))
                    IsBusy = !_isNotBusy;
            }
        }

        public DelegateCommand DebugCommnad { get; }

        public ViewModelBase(INavigationService navigationService, IPageDialogService pageDialogService)
        {
            NavigationService = navigationService;
            PageDialogService = pageDialogService;

            DebugCommnad = new DelegateCommand(ExecuteDebugCommand, CanExecuteDebugCommand);
            DebugCommnad.RaiseCanExecuteChanged();

            _title = GetType().FullName.Replace("MedX.App.", string.Empty);
        }

        protected virtual bool CanExecuteDebugCommand() => false;

        protected virtual void ExecuteDebugCommand()
        {
            throw new NotImplementedException();
        }

        public virtual void OnNavigatedFrom(NavigationParameters parameters)
        {
            
        }

        public virtual void OnNavigatedTo(NavigationParameters parameters)
        {
            DebugCommnad.RaiseCanExecuteChanged();
        }

        public virtual void OnNavigatingTo(NavigationParameters parameters)
        {
            
        }

        public virtual void Destroy()
        {
            
        }

        protected static void PlaySound(string soundName)
        {
            // https://blog.xamarin.com/play-audio-and-video-with-the-mediamanager-plugin-for-xamarin/
            // https://blog.xamarin.com/adding-sound-xamarin-forms-app/

            // 
            // http://www.soundsboom.com/trend/well-done

            // When loading audio from a shared library, set the Build Action to Embedded Resource.

            var assembly = typeof(App).GetTypeInfo().Assembly;
            var audioStream = assembly.GetManifestResourceStream($"MedX.App.Assets.Sounds.{soundName}");

            if (audioStream == null)
            {
                throw new Exception($"{soundName} was not found.");
            }

            AudioPlayer?.Load(audioStream);
            AudioPlayer?.Play();
        }

        protected async void DisplayToBeDoneMessage(string title)
        {
            await PageDialogService.DisplayAlertAsync(title, "To.Be.Done", "OK");
        }

        private static readonly List<Type> ShowDebugNotificationList = new List<Type>();

        protected void DoNotShowDebugNotifications()
        {
            ShowDebugNotificationList.Add(GetType());
        }
        protected async void ShowDebugNotification(string message, bool showOnce)
        {
            if (ShowDebugNotificationList.Contains(GetType()))
                return;

            if (showOnce)
            {
                DoNotShowDebugNotifications();
            }

            var notificator = Xamarin.Forms.DependencyService.Get<IToastNotificator>();
            var options = new NotificationOptions()
            {
                Title = "MedX.App Prototype",
                Description = message,
                ClearFromHistory = true,
                
                // Set to true if you want the result Clicked to come back(if the user clicks it)
                IsClickable = true,
                AndroidOptions = new AndroidOptions()
                {
                    HexColor = "#42F47A",
                    ForceOpenAppOnNotificationTap = false
                }
            };

            if (notificator != null)
            {
                await notificator.Notify(options);
            }
        }


    }
}
