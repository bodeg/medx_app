﻿using MedX.App.ViewModels.Base;
using MedX.App.ViewModels.Ble;
using MedX.App.ViewModels.Demo;
using MedX.App.ViewModels.Session;

namespace MedX.App.ViewModels
{
    internal class DesignTimeViewModelLocator
    {
        public static BasePageViewModel BasePageViewModel => null;
        public static EntryPopupViewModel EntryPopupViewModel => null;

        public static MainPageViewModel MainPageViewModel => null;
        public static DemoBasePageViewModel DemoBasePageViewModel => null;
        public static DemoPageViewModel DemoPageViewModel => null;

        public static LoginPageViewModel LoginPageViewModel => null;
        public static HomePageViewModel HomePageViewModel => null;

        #region session

        public static BaseSessionViewModel BaseSessionViewModel => null;
        public static PainLevelSurveyViewModel PainLevelSurveyViewModel => null;
        public static BaseGoalViewModel BaseGoalViewModel => null;
        public static CurrGoalViewModel CurrGoalViewModel => null;
        public static ReasonToReduceViewModel ReasonToReduceViewModel => null;
        public static PrevGoalViewModel PrevGoalViewModel => null;
        public static CustomGoalViewModel CustomGoalViewModel => null;
        public static MachineSetUpViewModel MachineSetUpViewModel => null;
        public static WorkoutViewModel WorkoutViewModel => null;
        public static WorkoutSummaryViewModel WorkoutSummaryViewModel => null;
        public static ScoresViewModel ScoresViewModel => null;
        public static MachineSetUpConfigViewModel MachineSetUpConfigViewModel => null;

        #endregion

        #region ble

        public static BaseBleViewModel BaseBleViewModel => null;
        public static BleInfoViewModel BleInfoViewModel => null; 
        public static BleScanViewModel BleScanViewModel => null;
        public static CalibrationViewModel CalibrationViewModel => null;       

        #endregion
    }
}