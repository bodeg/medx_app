﻿using MedX.App.Models;
using MedX.App.Services;
using Prism.Navigation;
using Prism.Services;

namespace MedX.App.ViewModels.Session
{
    public class CurrGoalViewModel : BaseGoalViewModel
    {
        protected override string YesText => "Yes, I feel comfortable with my goal";
        protected override string NoText => "No, I want to reduce my goal";
        public override Goal Goal => DailySession.Instance.CurrGoal;
        protected override string RedusedGoalPage => Navigation.Session.ReasonToReduce;

        public CurrGoalViewModel(INavigationService navigationService, IPageDialogService pageDialogService)
            : base(navigationService, pageDialogService)
        {
            HeaderText = @"Today's Goal";
            InstructionsText = @"Do you feel comfortable with today's goal?";

            MachineSetUpViewModel.ShowReducedGoalSystemMessage = true;
        }
    }
}