﻿using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using MedX.App.Helpers;
using MedX.App.Models;
using MedX.App.ViewModels.Base;
using Prism.Commands;
using Prism.Navigation;
using Prism.Services;

namespace MedX.App.ViewModels.Session
{
    public class BaseSessionViewModel : BasePageViewModel
    {
        public ObservableCollection<FlowListItem> FlowItems { get; } = new ObservableCollection<FlowListItem>();
        public int FlowItemCount => FlowItems?.Count ?? 0;

        private string _bgImage = "background_korkovado.png";
        private string _headerText;
        private string _instructionsText;
        private string _backButtonText = Strings.BackButtonText;
        private string _nextButtonText = Strings.NextButtonText;

        public string BgImage
        {
            get => _bgImage;
            set => SetProperty(ref _bgImage, value);
        }
        
        public string HeaderText
        {
            get => _headerText;
            set => SetProperty(ref _headerText, value);
        }

        public string InstructionsText
        {
            get => _instructionsText;
            set => SetProperty(ref _instructionsText, value);
        }

        public string BackButtonText
        {
            get => _backButtonText;
            protected set => SetProperty(ref _backButtonText, value);
        }

        public string NextButtonText
        {
            get => _nextButtonText;
            protected set => SetProperty(ref _nextButtonText, value);
        }

        public DelegateCommand PlaySoundCommand { get; }
        public DelegateCommand BackCommand { get; }
        public DelegateCommand NextCommand { get; }

        public BaseSessionViewModel(INavigationService navigationService, IPageDialogService pageDialogService)
            : base(navigationService, pageDialogService)
        {
            PlaySoundCommand = new DelegateCommand(PlaySound);

            BackCommand = new DelegateCommand(ExecuteBackCommand, CanExecuteBackCommand);
            NextCommand = new DelegateCommand(ExecuteNextCommand, CanExecuteNextCommand);
        }

        protected static Xamarin.Forms.Color SelectedFlowListItemColor { get; } = Xamarin.Forms.Color.Lime;
        protected static Xamarin.Forms.Color UnSelectedFlowListItemColor { get; } = Xamarin.Forms.Color.LightGray;
        protected void InitializeFlowListView(int count, int step, bool isLabelVisible)
        {
            FlowItems.Clear();

            for (var i = 0; i < count; i++)
            {
                var item = new FlowListItem
                {
                    Value = isLabelVisible ? $"{i + 1}" : string.Empty,
                    BackgroundColor = i < step 
                        ? SelectedFlowListItemColor
                        : UnSelectedFlowListItemColor
                };

                FlowItems.Add(item);
            }

            RaisePropertyChanged(nameof(FlowItemCount));
        }


        protected virtual async void PlaySound()
        {
            try
            {
                if (AudioPlayer.IsPlaying)
                {
                    AudioPlayer?.Stop();
                }
                else
                {
                    const string soundName = "on_a_mission_from_god.mp3";
                    PlaySound(soundName);
                }
            }
            catch (Exception exception)
            {
                await PageDialogService.DisplayAlertAsync("PlaySound", exception.Message, "OK");
            }
        }
        
        protected virtual async void ExecuteBackCommand()
        {
            await Task.Delay(TimeSpan.FromMilliseconds(0));
            throw new NotImplementedException();
        }

        protected virtual async void ExecuteNextCommand()
        {
            await Task.Delay(TimeSpan.FromMilliseconds(0));
            throw new NotImplementedException();
        }

        protected virtual bool CanExecuteBackCommand() => true;
        protected virtual bool CanExecuteNextCommand() => true;

        protected async void NavigateAsyncToHomePage()
        {
            await NavigationService.NavigateAsync($"/NavigationPage/{Navigation.General.Home}");
        }
    }
}