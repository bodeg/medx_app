﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using MedX.App.Models;
using MedX.App.Views;
using Prism.Navigation;
using Prism.Services;

namespace MedX.App.ViewModels.Session
{
	public class ReasonToReduceViewModel : BaseSessionViewModel
	{
	    private Answer _selectedAnswer;

        public ObservableCollection<Answer> Reasons { get; } = new ObservableCollection<Answer>();

	    public Answer SelectedAnswer
        {
	        get => _selectedAnswer;
	        set => SetProperty(ref _selectedAnswer, value);
	    }

        public ReasonToReduceViewModel(INavigationService navigationService, IPageDialogService pageDialogService)
            : base(navigationService, pageDialogService)
        {
            HeaderText = @"My Reason To Reduce My Goal Is";
            InstructionsText = string.Empty;

            foreach (var reason in new[]
            {
                @"I'm sore from last activity",
                @"My pain has increased",
                @"I've injured myself",
                @"I don't feel well"
            })
            {
                Reasons.Add(new Answer {Text = reason});
            }

            Reasons.Add(new Answer {Text = @"Other", AllowFreeText = true});
        }

	    protected override async void OnPropertyChanged(PropertyChangedEventArgs args)
	    {
	        base.OnPropertyChanged(args);

	        if (args.PropertyName.Equals(nameof(SelectedAnswer)))
	        {
	            NextCommand.RaiseCanExecuteChanged();

	            foreach (var answer in Reasons)
	            {
	                answer.IsSelected = answer == SelectedAnswer;
	            }

	            if (SelectedAnswer != null && SelectedAnswer.AllowFreeText)
	            {
	                await NavigationService.NavigateAsync(nameof(EntryPopupView),
	                    new NavigationParameters
	                    {
	                        {EntryPopupViewModel.Parameters.Title, "Custom Reason"},
	                        {EntryPopupViewModel.Parameters.SubTitle, $"{SelectedAnswer?.Text}:"},
	                        {EntryPopupViewModel.Parameters.Text, SelectedAnswer?.FreeText},
	                        {EntryPopupViewModel.Parameters.AcceptButtonText, "Close"}
	                    });
	            }
	        }
        }

        public override void OnNavigatedTo(NavigationParameters parameters)
	    {
	        base.OnNavigatedTo(parameters);

	        if (_selectedAnswer != null && parameters.ContainsKey(EntryPopupViewModel.Parameters.Text))
	        {
	            _selectedAnswer.FreeText = parameters.GetValue<string>(EntryPopupViewModel.Parameters.Text);
	        }
	    }
	    
        protected override async void ExecuteBackCommand()
        {
            await NavigationService.GoBackAsync();
        }

        protected override async void ExecuteNextCommand()
        {
            await NavigationService.NavigateAsync(Navigation.Session.PrevGoal);
        }
	    protected override bool CanExecuteNextCommand()
	    {
	        return SelectedAnswer != null && base.CanExecuteNextCommand();
	    }
    }
}
