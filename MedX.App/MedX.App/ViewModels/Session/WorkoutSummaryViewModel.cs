﻿using System;
using MedX.App.Helpers;
using MedX.App.Views;
using Prism.Commands;
using Prism.Navigation;
using Prism.Services;
using MedX.App.Views.Session;

namespace MedX.App.ViewModels.Session
{
    public class WorkoutSummaryViewModel : BaseSessionViewModel
    {
        private string _messageText;
        private bool _isMessageSent;

        public bool IsMessageSent
        {
            get => _isMessageSent;
            set => SetProperty(ref _isMessageSent, value);
        }

        public DelegateCommand SendMessageCommand { get; }

        public WorkoutSummaryViewModel(INavigationService navigationService, IPageDialogService pageDialogService)
            : base(navigationService, pageDialogService)
        {
            HeaderText = @"Workout Summary";
            InstructionsText = string.Empty;
            BackButtonText = Strings.BackButtonText;
            NextButtonText = Strings.NextButtonText;

            SendMessageCommand = new DelegateCommand(SendMessage);
        }

        protected async void SendMessage()
        {
            await NavigationService.NavigateAsync(Navigation.General.EntryPopup,
                new NavigationParameters
                {
                    {EntryPopupViewModel.Parameters.Title, $"{HeaderText}"},
                    {EntryPopupViewModel.Parameters.SubTitle, "To: Aaron Rogers"},
                    {EntryPopupViewModel.Parameters.Text, _messageText},
                    {EntryPopupViewModel.Parameters.AcceptButtonText, "SEND"},
                    {EntryPopupViewModel.Parameters.CancelButtonText, "CANCEL"}
                });
        }

        public override void OnNavigatedTo(NavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);

            if (parameters.ContainsKey(EntryPopupViewModel.Parameters.Text))
            {
                _messageText = parameters.GetValue<string>(EntryPopupViewModel.Parameters.Text);
                _messageText = _messageText?.Trim();

                if (!string.IsNullOrEmpty(_messageText))
                {
                    IsMessageSent = true;
                    PageDialogService.DisplayAlertAsync(@"System Message:", "Message has been sent.", "OK");
                }
            }
        }

        protected override bool CanExecuteBackCommand() => false;

        protected override void ExecuteBackCommand()
        {
            throw new NotImplementedException();
        }

        protected override async void ExecuteNextCommand()
        {
            await NavigationService.NavigateAsync(Navigation.Session.Scores);
        }
    }
}