﻿using MedX.App.Helpers;
using MedX.App.Models;
using MedX.App.Services;
using Prism.Navigation;
using Prism.Services;

namespace MedX.App.ViewModels.Session
{
    public class CustomGoalViewModel : BaseSessionViewModel
    {
        public Goal Goal => DailySession.Instance.CustomGoal;
        public Goal CurrGoal => DailySession.Instance.CurrGoal;
        public CustomGoalViewModel(INavigationService navigationService, IPageDialogService pageDialogService)
            : base(navigationService, pageDialogService)
        {
            HeaderText = @"Choose your own goal for today";
            InstructionsText = string.Empty;

            //BackButtonText = "Back";
            NextButtonText = Strings.FinishButtonText;
        }

        public override void OnNavigatingTo(NavigationParameters parameters)
        {
            base.OnNavigatingTo(parameters);

            if (DailySession.Instance.CustomGoal == null)
            {
                DailySession.Instance.CustomGoal = DailySession.Instance.CurrGoal.Clone();
            }
        }

        protected override async void ExecuteBackCommand()
        {
            await NavigationService.GoBackAsync();
        }

        protected override async void ExecuteNextCommand()
        {
            DailySession.Instance.ActualGoal = Goal.Clone();
            await NavigationService.NavigateAsync(Navigation.Session.MachineSetUp);
        }
    }
}