﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Text;
using MedX.App.Helpers;
using MedX.App.Models;
using MedX.App.Services;
using Prism.Navigation;
using Prism.Services;

namespace MedX.App.ViewModels.Session
{
    public class BaseGoalViewModel : BaseSessionViewModel
    {
        private Answer _selectedAnswer;

        public ObservableCollection<Answer> Answers { get; } = new ObservableCollection<Answer>();

        public Answer SelectedAnswer
        {
            get => _selectedAnswer;
            set => SetProperty(ref _selectedAnswer, value);
        }

        protected virtual string YesText => "Yes";
        protected virtual string NoText => "No";
        public virtual Goal Goal { get; } = null;
        protected virtual string RedusedGoalPage => throw new NotImplementedException();

        public BaseGoalViewModel(INavigationService navigationService, IPageDialogService pageDialogService)
            : base(navigationService, pageDialogService)
        {
            RaisePropertyChanged(nameof(UseSpecialYesNoButtons));
        }

        protected override void OnPropertyChanged(PropertyChangedEventArgs args)
        {
            base.OnPropertyChanged(args);

            if (args.PropertyName.Equals(nameof(SelectedAnswer)))
            {
                NextCommand.RaiseCanExecuteChanged();

                foreach (var answer in Answers)
                {
                    answer.IsSelected = answer == SelectedAnswer;
                }
            }
            else if (args.PropertyName.Equals(nameof(UseSpecialYesNoButtons)))
            {
                if (UseSpecialYesNoButtons)
                {
                    BackButtonText = Strings.BackButtonText;
                    NextButtonText = Strings.NextButtonText;
                }
                else
                {
                    BackButtonText = "No";
                    NextButtonText = "Yes";
                }

                NextCommand.RaiseCanExecuteChanged();
            }
        }

        public override void OnNavigatingTo(NavigationParameters parameters)
        {
            base.OnNavigatingTo(parameters);

            if (Answers.Count == 0)
            {
                Answers.Add(new Answer {Text = YesText, Value = true});
                Answers.Add(new Answer {Text = NoText, Value = false});
            }
        }

        protected virtual async void NavigateToMachineSetUpPage()
        {
            DailySession.Instance.ActualGoal = Goal.Clone();
            await NavigationService.NavigateAsync(Navigation.Session.MachineSetUp);
        }

        protected virtual async void NavigateToRedusedGoalPage()
        {
            await NavigationService.NavigateAsync(RedusedGoalPage);
        }

        protected override async void ExecuteBackCommand()
        {
            if (UseSpecialYesNoButtons)
            {
                await NavigationService.GoBackAsync();
            }
            else
            {
                NavigateToRedusedGoalPage();
            }
        }

        protected override void ExecuteNextCommand()
        {
            if (UseSpecialYesNoButtons)
            {
                if (SelectedAnswer == null) return;

                if ((bool) SelectedAnswer.Value)
                {
                    NavigateToMachineSetUpPage();
                }
                else
                {
                    NavigateToRedusedGoalPage();
                }
            }
            else
            {
                // yes!
                NavigateToMachineSetUpPage();
            }
        }

        protected override bool CanExecuteNextCommand()
        {
            return UseSpecialYesNoButtons
                ? SelectedAnswer != null && base.CanExecuteNextCommand()
                : base.CanExecuteNextCommand();
        }

        #region DebugCommand

        private static bool _useSpecialYesNoButtons = true;

        public bool UseSpecialYesNoButtons
        {
            get => _useSpecialYesNoButtons;
            set => SetProperty(ref _useSpecialYesNoButtons, value);
        }

        protected override bool CanExecuteDebugCommand() => true;

        protected override async void ExecuteDebugCommand()
        {
            var message = new StringBuilder();
            message.AppendLine(@"Use the navigation buttons only for:");
            message.AppendLine(@"'Back' or 'Next'");

            var result = await PageDialogService.DisplayAlertAsync("Navigation Settings",
                message.ToString(), 
                acceptButton: UseSpecialYesNoButtons ? "YES(as-now)" : "YES", 
                cancelButton: UseSpecialYesNoButtons ? "NO" : "NO(as-now)");

            UseSpecialYesNoButtons = result;
        }

        #endregion
    }
}