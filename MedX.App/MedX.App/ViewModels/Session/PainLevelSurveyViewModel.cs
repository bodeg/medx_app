﻿using System.Collections.ObjectModel;
using System.Linq;
using MedX.App.Helpers;
using MedX.App.Models;
using Prism.Navigation;
using Prism.Services;

namespace MedX.App.ViewModels.Session
{
    public class PainLevelSurveyViewModel : BaseSessionViewModel
    {
        private int _painLevel;
        public int MinPainLevel { get; } = 0;
        public int MaxPainLevel { get; } = 10;
        public ObservableCollection<FlowListItem> PainLevelImages { get; } = new ObservableCollection<FlowListItem>(); 
        public int PainLevel
        {
            get => _painLevel;
            set => SetProperty(ref _painLevel, value);
        }

        public PainLevelSurveyViewModel(INavigationService navigationService, IPageDialogService pageDialogService)
            : base(navigationService, pageDialogService)
        {
            HeaderText = @"Pain Level Survey";
            InstructionsText = @"On the scale below, please rate the worst your pain has been in the last week";
            BackButtonText = Strings.BackButtonText;
            NextButtonText = Strings.NextButtonText;

            for (var painLevel = MaxPainLevel; painLevel >= MinPainLevel; painLevel--)
            {
                PainLevelImages.Add(new FlowListItem { Value = null });
            }

            PainLevelImages.Last().Value = MinPainLevel;

            PropertyChanged += (sender, args) =>
            {
                if (args.PropertyName.Equals(nameof(PainLevel)))
                {
                    var index = 0;
                    for (var painLevel = MaxPainLevel; painLevel >= MinPainLevel; painLevel--, index++)
                    {
                        PainLevelImages[index].Value = PainLevel == painLevel ? painLevel : (object) null;
                    }
                }
            };
        }

        protected override async void ExecuteBackCommand()
        {
            await NavigationService.GoBackAsync();
        }

        protected override async void ExecuteNextCommand()
        {
            await NavigationService.NavigateAsync(Navigation.Session.CurrGoal);
        }
    }
}
