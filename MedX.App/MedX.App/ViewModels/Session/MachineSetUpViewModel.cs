﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Text;
using MedX.App.Helpers;
using MedX.App.Models;
using MedX.App.Services;
using Prism.Commands;
using Prism.Navigation;
using Prism.Services;

namespace MedX.App.ViewModels.Session
{
    public class MachineSetUpViewModel : BaseSessionViewModel
    {
        protected ObservableCollection<MachineSetupItem> MachineSetUp =>
            MachineSetup.Instance(DailySession.Instance.Type).Steps;

        public static bool ShowReducedGoalSystemMessage { get; set; } = false;

        private int _stepIndex;
        private string _imageSource;

        public int StepIndex
        {
            get => _stepIndex;
            set => SetProperty(ref _stepIndex, value);
        }

        public string ImageSource
        {
            get => _imageSource;
            set => SetProperty(ref _imageSource, value);
        }

        public bool IsLastMachineSetUpStep => StepIndex + 1 == MachineSetUp.Count;

        public DelegateCommand SkipCommand { get; }


        public MachineSetUpViewModel(INavigationService navigationService, IPageDialogService pageDialogService)
            : base(navigationService, pageDialogService)
        {
            HeaderText = @"Machine Set Up";

            SkipCommand = new DelegateCommand(Skip);
        }

        protected override void OnPropertyChanged(PropertyChangedEventArgs args)
        {
            base.OnPropertyChanged(args);

            if (args.PropertyName.Equals(nameof(StepIndex)))
            {
                RaisePropertyChanged(nameof(IsLastMachineSetUpStep));
            }
        }

        private async void Skip()
        {
            await NavigationService.NavigateAsync(Navigation.Session.Workout);
        }

        protected override async void ExecuteBackCommand()
        {
            var navigationParameters = StepIndex > 0
                ? new NavigationParameters {{Parameters.StepIndex, StepIndex - 1}}
                : null;

            await NavigationService.GoBackAsync(navigationParameters);
        }

        protected override async void ExecuteNextCommand()
        {
            if (StepIndex < MachineSetUp.Count - 1)
            {
                var navigationParameters = new NavigationParameters {{Parameters.StepIndex, StepIndex + 1}};
                await NavigationService.NavigateAsync(Navigation.Session.MachineSetUp, navigationParameters);
            }
            else
            {
                await NavigationService.NavigateAsync(Navigation.Session.Workout);
            }
        }

        public struct Parameters
        {
            public static string StepIndex { get; } = $"{nameof(MachineSetUpViewModel)}.{nameof(StepIndex)}";
        }

        private void InitializeComponent(NavigationParameters parameters)
        {
            StepIndex = parameters.ContainsKey(Parameters.StepIndex)
                ? parameters.GetValue<int>(Parameters.StepIndex)
                : 0;

            StepIndex = Math.Max(StepIndex, 0);
            StepIndex = Math.Min(StepIndex, MachineSetUp.Count - 1);

            var currSetUp = MachineSetUp[StepIndex];

            var instructionsText = currSetUp.Text;
            instructionsText = instructionsText.Replace("#piston.color#", $"{DailySession.Instance.ActualGoal.PistonColor}");
            instructionsText = instructionsText.Replace("#piston.settings#", $"{DailySession.Instance.ActualGoal.PistonSettings}.00");
            instructionsText = instructionsText.Replace("#rom.limiter#", $"{DailySession.Instance.ActualGoal.ExtensionLimiter}");
            instructionsText = instructionsText.Replace("#rom.flex#", $"{DailySession.Instance.ActualGoal.FlexionPosition}");

            InstructionsText = instructionsText;
            ImageSource = currSetUp.Image;
            BackButtonText = Strings.BackButtonText;
            NextButtonText = StepIndex < MachineSetUp.Count - 1 ? Strings.NextButtonText : Strings.FinishButtonText;

            InitializeFlowListView(MachineSetUp.Count, StepIndex + 1, isLabelVisible: false);
        }

        public override void OnNavigatingTo(NavigationParameters parameters)
        {
            base.OnNavigatingTo(parameters);

            InitializeComponent(parameters);
        }

        public override void OnNavigatedTo(NavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);

            if (!DailySession.Instance.CurrGoal.IsEqual(DailySession.Instance.ActualGoal) &&
                ShowReducedGoalSystemMessage)
            {
                ShowReducedGoalSystemMessage = false;

                var message = new StringBuilder();
                message.AppendLine("Patients change their goals for many reasons.");
                message.AppendLine(
                    "If you are concerned about your condition, please contact your personal clinician today.");

                PageDialogService.DisplayAlertAsync(Strings.SystemMessageTitle, message.ToString(),
                    Strings.AlertCloseButtonText);
            }

            var navigatedFrom = parameters.GetValue<string>(Navigation.General.NavigatedFrom);

            if (!string.IsNullOrEmpty(navigatedFrom) && navigatedFrom.Equals(nameof(MachineSetUpConfigViewModel)))
            {
                InitializeComponent(parameters);
            }
        }

        #region DebugCommand

        protected override bool CanExecuteDebugCommand() => true;

        protected override async void ExecuteDebugCommand()
        {
            var name = $"/NavigationPage/{Navigation.Session.MachineSetUp}/{Navigation.Session.MachineSetUpConfig}";
            await NavigationService.NavigateAsync(name);
        }

        #endregion
    }
}