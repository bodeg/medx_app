﻿using System;
using System.Diagnostics;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MedX.App.Helpers;
using MedX.App.Models;
using MedX.App.Services;
using Prism.Navigation;
using Prism.Services;

// FFImageLoading
// https://github.com/luberda-molinet/FFImageLoading/wiki/Xamarin.Forms-API

// Usage in ListView with ListViewCachingStrategy.RecycleElement enabled
// https://github.com/luberda-molinet/FFImageLoading/wiki/Xamarin.Forms-Advanced

namespace MedX.App.ViewModels.Session
{
    public class WorkoutViewModel : BaseSessionViewModel
    {
        public enum WorkoutState
        {
            None,
            Forward,
            Backward,
            Hold,
            Release
        }

        public static WorkoutState ReleaseState { get; } = WorkoutState.Release;

        private WorkoutState _state = WorkoutState.None;
        private int _imageIndex;
        private string _imageSource;
        private int _releasing;
        private int _repetitions;
        private CancellationTokenSource _cancellationTokenSource;

        public WorkoutState State
        {
            get => _state;
            set => SetProperty(ref _state, value);
        }

        public int ImageIndex
        {
            get => _imageIndex;
            set => SetProperty(ref _imageIndex, value);
        }

        public string ImageSource
        {
            get => _imageSource;
            set => SetProperty(ref _imageSource, value);
        }

        public int Releasing
        {
            get => _releasing;
            set => SetProperty(ref _releasing, value);
        }

        public int Repetitions
        {
            get => _repetitions;
            set
            {
                if (!SetProperty(ref _repetitions, value)) return;

                for (var i = 0; i < FlowItems.Count; i++)
                {
                    FlowItems[i].BackgroundColor = i < _repetitions
                        ? SelectedFlowListItemColor
                        : UnSelectedFlowListItemColor;
                }
            }
        }

        private int MaxRepetitions => DailySession.Instance.ActualGoal.Repetitions;
        private static int NoneIndex { get; } = 0;
        private static int FirstForwardIndex { get; } = 0;
        private static int LastForwardIndex { get; } = 6;
        private static int FirstBackwardIndex { get; } = 7;
        private static int LastBackwardIndex { get; } = 12;


        #region sfGauge

        private const int NullValue = -1;

        private int _value = NullValue;
        private int _minValue = NullValue;
        private int _maxValue = NullValue;

        //public Goal Goal => DailySession.Instance.ActualGoal;
        public int ExtensionLimiter => 4;
        public int FlexionPosition => 10;

        public int Value
        {
            get => _value;
            set => SetProperty(ref _value, value);
        }

        public int MinValue
        {
            get => _minValue;
            set => SetProperty(ref _minValue, value);
        }

        public int MaxValue
        {
            get => _maxValue;
            set => SetProperty(ref _maxValue, value);
        }

        #endregion

        public WorkoutViewModel(INavigationService navigationService, IPageDialogService pageDialogService)
            : base(navigationService, pageDialogService)
        {
            HeaderText = @"Workout";
            InstructionsText = $@"Repeat Steps 3-5 for total of {MaxRepetitions} times";
            BackButtonText = Strings.BackButtonText;
            NextButtonText = Strings.FinishButtonText;

            InitializeFlowListView(MaxRepetitions, 0, isLabelVisible: true);

            ImageSource = $"workout_{ImageIndex:D2}.png";
        }

        protected override async void ExecuteBackCommand()
        {
            await NavigationService.GoBackAsync();
        }

        protected override async void ExecuteNextCommand()
        {
            if (_workouts == 0)
            {
                var message = new StringBuilder();
                message.AppendLine("You may press the Green-Bug to simulate a workout.");
                message.AppendLine("Do you want to give it a try?");

                var result = await PageDialogService.DisplayAlertAsync(Strings.AppTitle,
                    message.ToString(), acceptButton: "Yes", cancelButton: "No");

                if (result)
                    return;
            }

            DailySession.Instance.Status = _workouts > 0 ? SessionStatus.Completed : SessionStatus.Incomplete;

            await NavigationService.NavigateAsync($"/NavigationPage/{Navigation.Session.WorkoutSummary}");
        }

        #region DebugCommand

        private int _workouts = 0;
        private static bool _showDebugNotification = !Debugger.IsAttached;

        protected override bool CanExecuteDebugCommand() => true;

        protected override async void ExecuteDebugCommand()
        {
            DoNotShowDebugNotifications();

            if (_cancellationTokenSource != null)
            {
                _cancellationTokenSource.Cancel();
                return;
            }

            if (_showDebugNotification)
            {
                _showDebugNotification = false;
                await PageDialogService.DisplayAlertAsync("To.Be.Done",
                    @"Improve animation and avoid flickering.", "START");
            }

            _workouts++;

            using (_cancellationTokenSource = new CancellationTokenSource())
            {
                State = WorkoutState.Backward;
                ImageIndex = FirstForwardIndex;
                Repetitions = 1;

                var increment = 1;

                while (!_cancellationTokenSource.IsCancellationRequested)
                {
                    ImageSource = $"workout_{ImageIndex:D2}.png";

                    if (Value == NullValue)
                    {
                        Value = ExtensionLimiter;
                    }
                    else
                    {
                        if (Value <= ExtensionLimiter)
                            increment = +1;
                        else if (Value >= FlexionPosition)
                            increment = -1;

                        Value += increment;
                    }

                    MinValue = MinValue == NullValue ? Value : Math.Min(MinValue, Value);
                    MaxValue = MaxValue == NullValue ? Value : Math.Max(MaxValue, Value);

                    if (State == WorkoutState.Forward && ImageIndex == LastForwardIndex)
                    {
                        PlaySound("bell_short.mp3");

                        State = WorkoutState.Backward;
                        ImageIndex = FirstBackwardIndex;
                    }
                    else if (State == WorkoutState.Backward && ImageIndex == LastBackwardIndex)
                    {
                        PlaySound("bell_long.mp3");

                        State = WorkoutState.Release;
                        Releasing = 1;
                    }
                    else if (State == WorkoutState.Release)
                    {
                        if (Releasing == 100)
                        {
                            Releasing = 0;

                            if (Repetitions == MaxRepetitions)
                            {
                                State = WorkoutState.None;
                                ImageIndex = NoneIndex;
                                PlaySound("well_done.mp3");
                            }
                            else
                            {
                                Repetitions++;
                                State = WorkoutState.Forward;
                                ImageIndex = FirstForwardIndex;
                            }
                        }
                        else
                        {
                            Releasing = Math.Min(Releasing + 5, 100);
                        }
                    }
                    else
                    {
                        ImageIndex++;
                    }

                    if (State == WorkoutState.None)
                    {
                        break;
                    }

                    if (!_cancellationTokenSource.IsCancellationRequested)
                    {
                        await Task.Delay(TimeSpan.FromMilliseconds(250));
                    }
                }
            }

            _cancellationTokenSource = null;

            State = WorkoutState.None;
            ImageIndex = NoneIndex;
            ImageSource = $"workout_{ImageIndex:D2}.png";
        }

        public override void OnNavigatedTo(NavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);

            if (_showDebugNotification)
            {
                ShowDebugNotification("Press the Green-Bug to start/stop.", false);
            }
        }

        public override void OnNavigatedFrom(NavigationParameters parameters)
        {
            _cancellationTokenSource?.Cancel();

            base.OnNavigatedFrom(parameters);
        }

        #endregion
    }
}