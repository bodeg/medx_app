﻿using System.Collections.ObjectModel;
using MedX.App.Models;
using MedX.App.Services;
using MedX.App.ViewModels.Base;
using Newtonsoft.Json;
using Plugin.Messaging;
using Prism.Commands;
using Prism.Navigation;
using Prism.Services;

namespace MedX.App.ViewModels.Session
{
    public class MachineSetUpConfigViewModel : BasePageViewModel
    {      
        public ObservableCollection<MachineSetupItem> Items =>
            MachineSetup.Instance(DailySession.Instance.Type).Steps;

        public DelegateCommand AddItemCommand { get; }
        public DelegateCommand<int?> EditItemCommand { get; }
        public DelegateCommand<int?> DeleteItemCommand { get; }

        public DelegateCommand RestoreCommand { get; }
        public DelegateCommand SendByEmailCommand { get; }

        public MachineSetUpConfigViewModel(INavigationService navigationService, IPageDialogService pageDialogService)
            : base(navigationService, pageDialogService)
        {
            Title = $"{DailySession.Instance.Type}";

            AddItemCommand = new DelegateCommand(AddItem);
            EditItemCommand = new DelegateCommand<int?>(EditItem);
            DeleteItemCommand = new DelegateCommand<int?>(DeleteItem);

            RestoreCommand = new DelegateCommand(Restore);
            SendByEmailCommand = new DelegateCommand(SendByEmail);
        }

        protected async void AddItem()
        {
           await NavigationService.NavigateAsync(Navigation.General.EntryPopup,
                new NavigationParameters
                {
                    {EntryPopupViewModel.Parameters.Title, $"New"},
                    {EntryPopupViewModel.Parameters.SubTitle, null},
                    {EntryPopupViewModel.Parameters.Text, string.Empty},
                    {EntryPopupViewModel.Parameters.AcceptButtonText, "CLOSE"},
                    {EntryPopupViewModel.Parameters.UserParam, -1},
                });
        }

        protected async void EditItem(int? index)
        {
            if (index == null) return;
            if (index < 0 || index >= Items.Count) return;

            await NavigationService.NavigateAsync(Navigation.General.EntryPopup,
                new NavigationParameters
                {
                    {EntryPopupViewModel.Parameters.Title, $"Edit ({index+1})"},
                    {EntryPopupViewModel.Parameters.SubTitle, null},
                    {EntryPopupViewModel.Parameters.Text, Items[index.Value].Text},
                    {EntryPopupViewModel.Parameters.AcceptButtonText, "CLOSE"},
                    {EntryPopupViewModel.Parameters.UserParam, index},
                });
        }

        protected async void DeleteItem(int? index)
        {
            if (index == null) return;

            const string message = @"Delete selected item?";
            var result = await PageDialogService.DisplayAlertAsync("Confirm", message, "YES", "NO");

            if (result && index >= 0 && index < Items.Count)
            {
                Items.RemoveAt(index.Value);
            }
        }

        private async void Restore()
        {
            const string message = @"Are you sure you want to restore the list to its factory settings?";
            var result = await PageDialogService.DisplayAlertAsync("Confirm", message, "YES", "NO");

            if (result)
            {
                MachineSetup.Instance(DailySession.Instance.Type).GetDefault();
            }
        }

        private async void SendByEmail()
        {
            var emailMessenger = CrossMessaging.Current.EmailMessenger;
            if (emailMessenger.CanSendEmail)
            {
                var email = new EmailMessageBuilder()
                    .To("bodeg.alon@gmail.com")
                    .Subject($"Machine Setup - {DailySession.Instance.Type}")
                    .Body(JsonConvert.SerializeObject(Items, Formatting.Indented))
                    .Build();

                emailMessenger.SendEmail(email);
            }
            else
            {
                await PageDialogService.DisplayAlertAsync("Email", "Cannot Send Mail.", "Close");
            }
        }

        public override void OnNavigatedTo(NavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);

            var index = parameters.GetValue<int?>(EntryPopupViewModel.Parameters.UserParam);
            if (index == null) return;

            var text = parameters.GetValue<string>(EntryPopupViewModel.Parameters.Text);
            text = text?.Trim();

            if (index == -1 && !string.IsNullOrEmpty(text))
            {
                Items.Add(new MachineSetupItem {Text = text});
            }
            else if (index >= 0 && index < Items.Count)
            {
                Items[index.Value].Text = text;
            }
        }

        public override void OnNavigatedFrom(NavigationParameters parameters)
        {
            MachineSetup.Instance(DailySession.Instance.Type).UpdateValue();

            parameters.Add(Navigation.General.NavigatedFrom, nameof(MachineSetUpConfigViewModel));
            base.OnNavigatedFrom(parameters);

        }
    }
}
