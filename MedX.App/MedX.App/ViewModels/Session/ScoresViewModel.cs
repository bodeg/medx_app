﻿using MedX.App.Helpers;
using MedX.App.Models;
using MedX.App.Services;
using Prism.Commands;
using Prism.Navigation;
using Prism.Services;

namespace MedX.App.ViewModels.Session
{
    public class ScoresViewModel : BaseSessionViewModel
    {
        public Goal TodaysGoal => DailySession.Instance.CurrGoal;
        public Goal ActualGoal => DailySession.Instance.ActualGoal;

        public DelegateCommand CompleteSummaryCommand { get; }        
        public ScoresViewModel(INavigationService navigationService, IPageDialogService pageDialogService)
            : base(navigationService, pageDialogService)
        {
            HeaderText = @"Scores";
            InstructionsText = "Well done";
            BackButtonText = Strings.BackButtonText;
            NextButtonText = Strings.FinishButtonText;

            CompleteSummaryCommand = new DelegateCommand(CompleteSummary);
        }

        protected void CompleteSummary()
        {
            DisplayToBeDoneMessage(@"COMPLETE SUMMARY");
        }

        protected override async void ExecuteBackCommand()
        {
            await NavigationService.GoBackAsync();
        }

        protected override void ExecuteNextCommand()
        {
            NavigateAsyncToHomePage();
        }
    }
}