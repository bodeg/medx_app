﻿using MedX.App.Models;
using MedX.App.Services;
using Prism.Navigation;
using Prism.Services;

namespace MedX.App.ViewModels.Session
{
    public class PrevGoalViewModel : BaseGoalViewModel
    {
        protected override string YesText => "Yes, I want to try it again";
        protected override string NoText => "No, I want to choose my own goal";
        public override Goal Goal => DailySession.Instance.PrevGoal;
        protected override string RedusedGoalPage => Navigation.Session.CustomGoal;

        public PrevGoalViewModel(INavigationService navigationService, IPageDialogService pageDialogService)
            : base(navigationService, pageDialogService)
        {
            HeaderText = @"Today's Goal";
            InstructionsText = @"You reached this goal yesterday, do you feel comfortable trying it again?";
        }
    }
}
