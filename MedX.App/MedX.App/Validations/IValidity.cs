﻿namespace MedX.App.Validations
{
    public interface IValidity
    {
        bool IsValid { get; set; }
    }

}