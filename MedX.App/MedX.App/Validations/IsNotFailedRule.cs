﻿namespace MedX.App.Validations
{
    public class IsNotFailedRule : IValidationRule<bool>
    {
        public string ValidationMessage { get; set; }

        public bool Check(bool value) => value;
    }
}