﻿namespace MedX.App.Validations
{
    // https://blog.xamarin.com/validation-xamarin-forms-enterprise-apps/
    public interface IValidationRule<in T>
    {
        string ValidationMessage { get; set; }

        bool Check(T value);
    }

}