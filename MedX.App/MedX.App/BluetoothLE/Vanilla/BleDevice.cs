﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MedX.App.BluetoothLE.Abstractions;
using MedX.App.BluetoothLE.Common;
using Plugin.BLE.Abstractions;
using Plugin.BLE.Abstractions.Contracts;
using Prism.Mvvm;

namespace MedX.App.BluetoothLE.Vanilla
{
    public class BleDevice : BindableBase, IBleDevice
    {
        private IDevice _device;

        public Guid Id => _device?.Id ?? Guid.Empty;
        public bool IsConnected => _device != null && _device.State == DeviceState.Connected;
        public int Rssi => _device?.Rssi ?? 0;
        public string Name => _device.Name ?? string.Empty;

        public BleDevice(IDevice device)
        {
            _device = device;
        }

        public void Dispose()
        {
            _device?.Dispose();
        }

        public void Update(IDevice device)
        {
            if (device != null)
            {
                _device = device;
            }

            RaisePropertyChanged(nameof(IsConnected));
            RaisePropertyChanged(nameof(Rssi));
        }

        public async Task<IBleService> GetServiceAsync(Guid serviceId, CancellationToken token)
        {
            var service = await _device.GetServiceAsync(serviceId, token);

            if (service == null)
            {
                throw new Exception($"BLE service ({serviceId}) not found.");
            }

            return new BleService(service);
        }
    }
}