﻿using System;
using System.Diagnostics;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MedX.App.BluetoothLE.Abstractions;
using MedX.App.BluetoothLE.Common;
using Plugin.BLE.Abstractions.Contracts;

namespace MedX.App.BluetoothLE.Vanilla
{
    public class BleService : IBleService
    {
        private readonly IService _service;

        public BleService(IService service)
        {
            _service = service;
        }

        public void Dispose()
        {
            _service?.Dispose();
        }

        public async Task<IBleCharacteristic> GetCharacteristicAsync(Guid characteristicId)
        {
            var characteristic = await _service.GetCharacteristicAsync(characteristicId);
            if (characteristic == null)
            {
                throw new Exception($"BLE characteristic {characteristicId} not found.");
            }

            return new BleCharacteristic(characteristic);
        }

        public async Task<string> ReadStringValueAsync(int characteristicIndex, CancellationToken token)
        {
            var characteristicId = BleUuid.Characteristic(characteristicIndex);
            var characteristic = await GetCharacteristicAsync(characteristicId);

            var stringValue = await characteristic.ReadStringValueAsync(token);
            return stringValue;
        }

        public async Task WriteStringValueAsync(int characteristicIndex, string stringValue, CancellationToken token)
        {
            var characteristicId = BleUuid.Characteristic(characteristicIndex);
            var characteristic = await GetCharacteristicAsync(characteristicId);

            await characteristic.WriteStringValueAsync(stringValue, token);
        }
    }
}