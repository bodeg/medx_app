﻿using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MedX.App.BluetoothLE.Abstractions;
using Plugin.BLE.Abstractions.Contracts;

namespace MedX.App.BluetoothLE.Vanilla
{
    public class BleCharacteristic : IBleCharacteristic
    {
        private readonly ICharacteristic _characteristic;

        public BleCharacteristic(ICharacteristic characteristic)
        {
            _characteristic = characteristic;
        }

        #region Implementation of IBleCharacteristic

        public Task<byte[]> ReadAsync(CancellationToken token)
        {
            if (!_characteristic.CanRead)
            {
                throw new Exception($"Characteristic {_characteristic.Id} cannot be read.");
            }

            return _characteristic.ReadAsync(token);
        }

        public async Task<string> ReadStringValueAsync(CancellationToken token)
        {
            if (!_characteristic.CanRead)
            {
                throw new Exception($"Characteristic {_characteristic.Id} cannot be read.");
            }

            //var stopwatch = Stopwatch.StartNew();

            var bytes = await _characteristic.ReadAsync(token);

            //stopwatch.Stop();
            //Debug.WriteLine($"{service.Id}/{characteristic.Id} read {bytes.Length} bytes - {stopwatch.ElapsedMilliseconds} ms.");

            var stringValue = Encoding.UTF8.GetString(bytes, 0, bytes.Length);
            stringValue = stringValue.TrimEnd('\0');

            return stringValue;
        }

        public async Task WriteStringValueAsync(string stringValue, CancellationToken token)
        {
            if (!_characteristic.CanWrite)
            {
                throw new Exception($"Characteristic {_characteristic.Id} cannot be write.");
            }

            var bytes = Encoding.UTF8.GetBytes(stringValue);
            var result = await _characteristic.WriteAsync(bytes, token);

            if (!result)
            {
                throw new Exception($"Failed to write string (Characteristic: {_characteristic.Id}).");
            }
        }

        #endregion
    }
}