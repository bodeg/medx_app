﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MedX.App.BluetoothLE.Abstractions;
using Plugin.BLE;
using Plugin.BLE.Abstractions;
using Plugin.BLE.Abstractions.Contracts;
using Prism.Mvvm;
using Xamarin.Forms;

namespace MedX.App.BluetoothLE.Vanilla
{
    public class BleAdapter : BindableBase, IBleAdapter
    {
        private static volatile BleAdapter _instance;
        private static readonly object SyncRoot = new object();

        public static BleAdapter Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (SyncRoot)
                    {
                        if (_instance == null)
                        {
                            _instance = new BleAdapter();
                        }
                    }
                }

                return _instance;
            }
        }

        private readonly IBluetoothLE _ble = CrossBluetoothLE.Current;
        private readonly IAdapter _adapter = CrossBluetoothLE.Current.Adapter;

        private bool _isDiscovering;
        private CancellationTokenSource _cancellationTokenSource;

        protected BleAdapter()
        {
            _ble.StateChanged += (sender, args) => RaisePropertyChanged(nameof(Status));

            _adapter.DeviceDiscovered += (sender, args) => AddOrUpdateDevice(args.Device);
        }

        public BleStatus Status
        {
            get
            {
                switch (_ble.State)
                {
                    case BluetoothState.Unknown:
                        return BleStatus.Unauthorized;
                    case BluetoothState.Unavailable:
                        return BleStatus.Unsupported;
                    case BluetoothState.Unauthorized:
                        return BleStatus.Unauthorized;
                    case BluetoothState.TurningOn:
                    case BluetoothState.On:
                        return BleStatus.On;
                    case BluetoothState.TurningOff:
                    case BluetoothState.Off:
                        return BleStatus.Off;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        public ObservableCollection<IBleDevice> Devices { get; } = new ObservableCollection<IBleDevice>();

        public bool IsDiscovering
        {
            get => _isDiscovering;
            protected set => SetProperty(ref _isDiscovering, value);
        }

        protected CancellationTokenSource CancellationTokenSource
        {
            get => _cancellationTokenSource;
            set
            {
                if (_cancellationTokenSource == value) return;

                _cancellationTokenSource?.Dispose();
                _cancellationTokenSource = value;
            }
        }

        public async void StartScanning()
        {
            try
            {
                if (IsPermissionGranted())
                {
                    Device.BeginInvokeOnMainThread(() => IsDiscovering = true);

                    CancellationTokenSource = new CancellationTokenSource();

                    Devices.Clear();

                    await StartScanningForDevicesAsync(CancellationTokenSource.Token);

                    Device.BeginInvokeOnMainThread(() => IsDiscovering = false);
                }
            }
            catch (Exception exception)
            {
                Debug.WriteLine($"Failed to discover: {exception.Message}");
            }
        }

        public void StopScanning()
        {
            if (CancellationTokenSource == null) return;

            CancellationTokenSource.Cancel();
            CancellationTokenSource = null;
            IsDiscovering = false;
        }

        protected bool IsPermissionGranted()
        {
            return true;

            /*
            const Permission permission = Permission.Location;
            var status = await CrossPermissions.Current.CheckPermissionStatusAsync(permission);

            if (status != PermissionStatus.Granted)
            {
                // Declaring and Handling Permissions Since Android API 23 Tutorial
                // https://www.numetriclabz.com/declaring-and-handling-permissions-since-android-api-23-tutorial-2/

                if (await CrossPermissions.Current.ShouldShowRequestPermissionRationaleAsync(permission))
                {
                    var message = new StringBuilder();
                    message.AppendLine($"{permission} permissions are needed to access the Bluetouth.");
                    message.AppendLine("Permissions not granted.");

                    MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message,
                        new MessagingServiceAlert
                        {
                            Title = "Application Permissions",
                            Message = message.ToString(),
                            Cancel = "OK"
                        });
                }

                var results = await CrossPermissions.Current.RequestPermissionsAsync(permission);
                //Best practice to always check that the key exists
                if (results.ContainsKey(permission))
                    status = results[permission];
            }

            if (status == PermissionStatus.Granted)
            {
                return true;
            }
            */
        }

        protected async Task StartScanningForDevicesAsync(CancellationToken token)
        {
            foreach (var connectedDevice in _adapter.ConnectedDevices)
            {
                try
                {
                    // update rssi for already connected evices (so tha 0 is not shown in the list)
                    await connectedDevice.UpdateRssiAsync();
                }
                catch (Exception exception)
                {
                    Debug.WriteLine($"Failed to update RSSI for {connectedDevice.Name}: {exception.Message}");
                }

                AddOrUpdateDevice(connectedDevice);
            }

            _adapter.ScanMode = ScanMode.LowLatency;
            await _adapter.StartScanningForDevicesAsync(null, null, false, token);
        }

        private void AddOrUpdateDevice(IDevice device)
        {
            Debug.WriteLine($"AddOrUpdateDevice({device.Name}, {device.Id})");

            if (Devices.FirstOrDefault(d => d.Id == device.Id) is BleDevice bleDevice)
            {
                bleDevice.Update(device);
            }
            else
            {
                Devices.Add(new BleDevice(device));
            }
        }

        public async Task<IBleDevice> Connect(Guid deviceId, CancellationToken token)
        {
            var device = await ConnectToKnownDeviceAsync(deviceId, token);
            return device;
        }

        protected async Task<IBleDevice> ConnectToKnownDeviceAsync(Guid deviceId, CancellationToken token)
        {
            var connectParameters = new ConnectParameters(autoConnect: false, forceBleTransport: false);
            var device = await _adapter.ConnectToKnownDeviceAsync(deviceId, connectParameters, token);
            //await DumpServicesAndCharacteristics(token);

            if (device == null)
                return null;

            AddOrUpdateDevice(device);

            return new BleDevice(device);
        }

    }
}