﻿using System;
using System.Collections.ObjectModel;
using System.Threading;
using System.Threading.Tasks;

namespace MedX.App.BluetoothLE.Abstractions
{
    public interface IBleAdapter
    {
        BleStatus Status { get; }
        ObservableCollection<IBleDevice> Devices { get; }
        bool IsDiscovering { get; }
        void StartScanning();
        void StopScanning();

        Task<IBleDevice> Connect(Guid deviceId, CancellationToken token);
    }
}
