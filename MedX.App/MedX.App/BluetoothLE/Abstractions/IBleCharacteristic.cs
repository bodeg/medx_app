﻿using System.Threading;
using System.Threading.Tasks;

namespace MedX.App.BluetoothLE.Abstractions
{
    public interface IBleCharacteristic
    {
        Task<byte[]> ReadAsync(CancellationToken token);
        Task<string> ReadStringValueAsync(CancellationToken token);
        Task WriteStringValueAsync(string stringValue, CancellationToken token);
    }
}