﻿namespace MedX.App.BluetoothLE.Abstractions
{
    public enum BleStatus
    {
        Unknown,
        Resetting,
        Unsupported,
        Unauthorized,
        Off,
        On
    }
}