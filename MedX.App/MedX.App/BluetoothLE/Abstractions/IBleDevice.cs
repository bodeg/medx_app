﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace MedX.App.BluetoothLE.Abstractions
{
    public interface IBleDevice : IDisposable
    {
        Guid Id { get; }
        bool IsConnected { get; }
        string Name { get; }
        int Rssi { get; }

        Task<IBleService> GetServiceAsync(Guid serviceId, CancellationToken token);
    }
}