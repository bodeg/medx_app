﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace MedX.App.BluetoothLE.Abstractions
{
    public interface IBleService : IDisposable
    {
        Task<IBleCharacteristic> GetCharacteristicAsync(Guid characteristicId);
        Task<string> ReadStringValueAsync(int characteristicIndex, CancellationToken token);
        Task WriteStringValueAsync(int characteristicIndex, string stringValue, CancellationToken token);
    }
}