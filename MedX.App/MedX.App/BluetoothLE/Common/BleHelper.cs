﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MedX.App.BluetoothLE.Abstractions;
using MedX.App.BluetoothLE.Vanilla;
using MedX.App.Services;

namespace MedX.App.BluetoothLE.Common
{
    public class BleHelper
    {
        protected IBleAdapter Adapter => BleAdapter.Instance;
        public Guid DeviceId { get; set; } = BleSetup.Instance.BleDeviceId;

        protected Task<IBleDevice> Connect(CancellationToken token)
        {
            if (Adapter.Status != BleStatus.On)
            {
                throw new Exception($"BLE adapter is {Adapter.Status}.");
            }

            if (DeviceId == Guid.Empty)
            {
                throw new Exception($"No paired BLE device was not paired ({DeviceId}).");
            }

            return Adapter.Connect(DeviceId, token);
        }
    }
}