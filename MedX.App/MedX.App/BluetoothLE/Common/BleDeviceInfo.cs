﻿using System.Diagnostics;
using Prism.Mvvm;

namespace MedX.App.BluetoothLE.Common
{
    public class BleDeviceInfo : BindableBase
    {
        private string _bleName;

        private string _companyName;
        private string _deviceName;
        private string _deviceSerialNo;

        private string _controlerVersion;
        private string _firmwareVersion;
        private string _controlerSerialNo;

        public string BleName
        {
            get => _bleName;
            set => SetProperty(ref _bleName, value);
        }

        public string CompanyName
        {
            get => _companyName;
            set => SetProperty(ref _companyName, value);
        }
        public string DeviceName
        {
            get => _deviceName;
            set => SetProperty(ref _deviceName, value);
        }
        public string DeviceSerialNo
        {
            get => _deviceSerialNo;
            set => SetProperty(ref _deviceSerialNo, value);
        }

        public string ControlerVersion
        {
            get => _controlerVersion;
            set => SetProperty(ref _controlerVersion, value);
        }

        public string FirmwareVersion
        {
            get => _firmwareVersion;
            set => SetProperty(ref _firmwareVersion, value);
        }

        public string ControlerSerialNo
        {
            get => _controlerSerialNo;
            set => SetProperty(ref _controlerSerialNo, value);
        }

        public void Dump()
        {
            Debug.WriteLine($"companyName = {CompanyName}.");
            Debug.WriteLine($"deviceName = {DeviceName}.");
            Debug.WriteLine($"deviceSerialNo = {DeviceSerialNo}.");

            Debug.WriteLine($"controlerVersion = {ControlerVersion}.");
            Debug.WriteLine($"firmwareVersion = {FirmwareVersion}.");
            Debug.WriteLine($"controlerSerialNo = {ControlerSerialNo}.");
        }
    }
}
