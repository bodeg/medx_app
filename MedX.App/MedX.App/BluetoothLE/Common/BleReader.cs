﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace MedX.App.BluetoothLE.Common
{
    public class BleReader : BleHelper
    {
        protected Guid ServiceId => BleUuid.ComboService;
        protected Guid CharacteristicId => BleUuid.Characteristic(1);
        protected int Interval => 100;

        public async Task Run(CancellationToken token)
        {
            using (var bleDevice = await Connect(token))
            {
                using (var service = await bleDevice.GetServiceAsync(ServiceId, token))
                {
                    var characteristic = await service.GetCharacteristicAsync(CharacteristicId);

                    await Task.Run(async () =>
                    {
                        try
                        {
                            var stopwatch = new Stopwatch();

                            while (!token.IsCancellationRequested)
                            {
                                token.ThrowIfCancellationRequested();

                                stopwatch.Restart();

                                var buffer = await characteristic.ReadAsync(token);

                                Device.BeginInvokeOnMainThread(() => OnReadValue(buffer));

                                stopwatch.Stop();

                                var delay = Interval - stopwatch.ElapsedMilliseconds;
                                if (!token.IsCancellationRequested && delay > 0)
                                {
                                    await Task.Delay(TimeSpan.FromMilliseconds(delay), token);
                                }
                            }
                        }
                        catch (Exception exception)
                        {
                            Debug.WriteLine($"{GetType().Name}: {exception.Message}");
                            throw;
                        }

                    }, token);
                }
            }
        }

        public event EventHandler<BleReaderEventArgs> ReadValue;

        protected void OnReadValue(byte[] buffer)
        {
            var args = new BleReaderEventArgs
            {
                Inclinometer = InclinometerValue(new[] {buffer[0], buffer[1]}),
                Strength = StrengthValue(new[] {buffer[2], buffer[3]})
            };

            var handler = ReadValue;
            handler?.Invoke(this, args);
        }

        public double? InclinometerValue(byte[] bytes)
        {
            if (bytes == null || bytes.Length != 2) return null;

            const double offset = 1024;
            const double sensitivity = 819;

            var voltage = BitConverter.IsLittleEndian
                ? BitConverter.ToUInt16(new[] {bytes[1], bytes[0]}, 0)
                : BitConverter.ToUInt16(new[] {bytes[0], bytes[1]}, 0);

            voltage = (ushort) (voltage / 32);

            var sine = (voltage - offset) / sensitivity;
            var radians = Math.Asin(sine);

            return double.IsNaN(radians)
                ? (double?) null
                : radians * 180.0 / Math.PI;
        }

        public double? StrengthValue(byte[] bytes)
        {
            if (bytes == null || bytes.Length != 2) return null;

            const double scale = 3.6 / 1024;
            const double slope = 1.0;
            const double offset = 0.0;

            var value = BitConverter.ToUInt16(new[] { bytes[1], bytes[0] }, 0);
            var voltage = value * scale * 1000;
            
            return slope * voltage + offset;
        }
    }

    public class BleReaderEventArgs : EventArgs
    {
        public double? Inclinometer { get; set; }
        public double? Strength { get; set; }
    }
}
