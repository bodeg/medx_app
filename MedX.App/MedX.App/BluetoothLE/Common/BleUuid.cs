﻿using System;
using System.Diagnostics;

namespace MedX.App.BluetoothLE.Common
{
    public static class BleUuid
    {
        internal static readonly Guid DeviceInformationService = ServiceGuid(1);
        internal static readonly Guid StrengthService = ServiceGuid(3);
        internal static readonly Guid InclinometerService = ServiceGuid(4);
        internal static readonly Guid ComboService = ServiceGuid(5);

        private static Guid ServiceGuid(int index) => ToUuid($"0033{index:D4}");
        internal static Guid Characteristic(int index) => ToUuid($"0033{index:D4}");

        private static Guid ToUuid(string value)
        {
            //          xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx
            //BaseUuid: 00000000-0000-1000-8000-00805F9B34FB
            //MedxUuid:         -0000-1000-8000-00805F9B34FB

            try
            {
                var guid = $"{value}-1212-efde-1523-785fef13d123";
                //guid = $"{value}-1010-efde-1523-785fef13d123";
                return Guid.Parse(guid);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                throw;
            }
        }
    }
}
