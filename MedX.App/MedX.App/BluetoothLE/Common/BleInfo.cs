﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace MedX.App.BluetoothLE.Common
{
    public class BleInfo : BleHelper
    {
        protected Guid ServiceId => BleUuid.DeviceInformationService;

        private static class CharacteristicIndex
        {
            public const int CompanyName = 1;
            public const int DeviceName = 2;
            public const int ControlerVersion = 3;
            public const int FirmwareVersion = 4;
            public const int ControlerSerialNo = 5;
            public const int DeviceSerialNo = 6;
        }

        public async Task<BleDeviceInfo> ReadDeviceInfoAsync(CancellationToken token)
        {
            using (var bleDevice = await Connect(token))
            {
                var deviceInfo = new BleDeviceInfo {BleName = bleDevice.Name};

                using (var service = await bleDevice.GetServiceAsync(ServiceId, token))
                {
                    deviceInfo.CompanyName = await service.ReadStringValueAsync(CharacteristicIndex.CompanyName, token);
                    deviceInfo.DeviceName = await service.ReadStringValueAsync(CharacteristicIndex.DeviceName, token);
                    deviceInfo.ControlerVersion = await service.ReadStringValueAsync(CharacteristicIndex.ControlerVersion, token);
                    deviceInfo.FirmwareVersion = await service.ReadStringValueAsync(CharacteristicIndex.FirmwareVersion, token);
                    deviceInfo.ControlerSerialNo = await service.ReadStringValueAsync(CharacteristicIndex.ControlerSerialNo, token);
                    deviceInfo.DeviceSerialNo = await service.ReadStringValueAsync(CharacteristicIndex.DeviceSerialNo, token);
                }

                return deviceInfo;
            }
        }

        public async Task WriteDeviceInfoAsync(BleDeviceInfo deviceInfo, CancellationToken token)
        {
            using (var bleDevice = await Connect(token))
            {
                using (var service = await bleDevice.GetServiceAsync(ServiceId, token))
                {
                    await service.WriteStringValueAsync(CharacteristicIndex.DeviceName, deviceInfo.DeviceName, token);
                    await service.WriteStringValueAsync(CharacteristicIndex.ControlerSerialNo, deviceInfo.ControlerSerialNo, token);
                    await service.WriteStringValueAsync(CharacteristicIndex.DeviceSerialNo, deviceInfo.DeviceSerialNo, token);
                }
            }
        }
    }
}