﻿using System;
using System.Diagnostics;
using System.Globalization;
using Xamarin.Forms;

namespace MedX.App.Converters
{
    // https://stackoverflow.com/questions/20707160/data-binding-int-property-to-enum-in-wpf

    public sealed class EnumAndNumberConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                return null;

            var result = value;

            try
            {
                if (IsEnum(targetType))
                {
                    // convert number to enum
                    var enumType = targetType.IsEnum ? targetType : Nullable.GetUnderlyingType(targetType);
                    result = Enum.ToObject(enumType, System.Convert.ToInt32(value));
                }
                else if (IsEnum(value.GetType()))
                {
                    // convert enum to number
                    result = System.Convert.ChangeType(value, Enum.GetUnderlyingType(value.GetType()));
                }
            }
            catch (Exception exception)
            {
                Debug.WriteLine($"[BidirectionalEnumAndNumberConverter] {exception.GetType()}: {exception.Message}");
                result = value;
            }

            return result;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            // perform the same conversion in both directions
            return Convert(value, targetType, parameter, culture);
        }

        private static bool IsEnum(Type type) => 
            type.IsEnum || Nullable.GetUnderlyingType(type)?.IsEnum == true;
    }
}