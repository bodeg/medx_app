﻿using System;
using System.Globalization;
using System.Linq;
using Xamarin.Forms;

namespace MedX.App.Converters
{
    public sealed class EnumMinValueConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                return null;

            if (value is Type enumType)
            {
            }
            else
            {
                enumType = value.GetType().IsEnum
                    ? value.GetType()
                    : Nullable.GetUnderlyingType(targetType);
            }

            return enumType.IsEnum
                    ? Enum.GetValues(enumType).Cast<int>().Min()
                    : value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}