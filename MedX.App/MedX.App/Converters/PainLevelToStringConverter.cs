﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace MedX.App.Converters
{
    public class PainLevelToStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is int)) return value;

            var painLevel = System.Convert.ToInt32(value);
            switch (painLevel)
            {
                case 0: return $@"{painLevel}. No Pain";
                case 1: return $@"{painLevel}. Minimal";
                case 2: return $@"{painLevel}. Mild";
                case 3: return $@"{painLevel}. Uncomfortable";
                case 4: return $@"{painLevel}. Moderate";
                case 5: return $@"{painLevel}. Distracting";
                case 6: return $@"{painLevel}. Distressing";
                case 7: return $@"{painLevel}. Unmanageable";
                case 8: return $@"{painLevel}. Intense";
                case 9: return $@"{painLevel}. Severe";
                case 10: return $@"{painLevel}. Unable to Move";
                default: return value;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

}