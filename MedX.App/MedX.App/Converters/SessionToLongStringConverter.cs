﻿using System;
using System.Globalization;
using MedX.App.Models;
using Xamarin.Forms;

namespace MedX.App.Converters
{
    public class SessionToLongStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is Session session)
            {
                var time = new TimeConverter().Convert(session.Time, typeof(string), null, culture);
                var type = new ToUpperConverter().Convert(session.Type, typeof(string), null, culture);

                return $"{time} - {type}";
            }

            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}