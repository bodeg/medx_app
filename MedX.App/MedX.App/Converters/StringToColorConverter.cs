﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace MedX.App.Converters
{
    public class StringToColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var colorString = value.ToString();
            return string.IsNullOrEmpty(colorString) 
                ? Xamarin.Forms.Color.Default 
                : Xamarin.Forms.Color.FromHex(colorString);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}