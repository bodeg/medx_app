﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace MedX.App.Converters
{
    public class ValueEqualsConverter : IValueConverter
    {
        public object Convert(object value, Type t, object parameter, CultureInfo culture)
        {
            return value != null ? value?.Equals(parameter) : false;
        }

        public object ConvertBack(object value, Type t, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}