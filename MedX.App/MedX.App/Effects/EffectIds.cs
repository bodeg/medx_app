﻿namespace MedX.App.Effects
{
    public class EffectIds
    {
        // "MedX.App.Effects.EntryLineColorEffect"
        public static string EntryLineColorEffect => typeof(EntryLineColorEffect).FullName;
        public static string ButtonNotAllCapsEffect => typeof(ButtonNotAllCapsEffect).FullName;
    }
}