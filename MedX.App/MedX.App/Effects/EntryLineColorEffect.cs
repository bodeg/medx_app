﻿using Xamarin.Forms;

namespace MedX.App.Effects
{
    public class EntryLineColorEffect : RoutingEffect
    {
        public EntryLineColorEffect() : base(EffectIds.EntryLineColorEffect)
        {
        }
    }
}
