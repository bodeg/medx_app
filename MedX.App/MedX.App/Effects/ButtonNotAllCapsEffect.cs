﻿using System;
using System.Collections.Generic;
using System.Text;
using MedX.App.Effects;
using Xamarin.Forms;

// Forms Community Toolkit
// https://github.com/FormsCommunityToolkit/FormsCommunityToolkit

// https://forums.xamarin.com/discussion/34817/button-on-android-all-caps

namespace MedX.App.Effects
{
    public class ButtonNotAllCapsEffect : RoutingEffect
    {
        public ButtonNotAllCapsEffect() : base(EffectIds.ButtonNotAllCapsEffect)
        {
        }
    }
}