﻿using System.Collections.ObjectModel;
using Prism.Mvvm;

namespace MedX.App.Models
{
    public class PickerCascadingData : BindableBase
    {
        private object _selectedItem;

        public ObservableCollection<object> MainCollection { get; }
        public ObservableCollection<string> Header { get; } = new ObservableCollection<string>();

        public ObservableCollection<object> MasterCollection { get; } = new ObservableCollection<object>();
        public ObservableCollection<object> DetailCollection { get; } = new ObservableCollection<object>();

        public object SelectedItem
        {
            get => _selectedItem;
            set => SetProperty(ref _selectedItem, value);
        }

        public PickerCascadingData()
        {
            MainCollection = new ObservableCollection<object> {MasterCollection, DetailCollection};
        }
    }
}