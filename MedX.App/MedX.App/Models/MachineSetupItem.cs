﻿using Prism.Mvvm;

namespace MedX.App.Models
{
    public class MachineSetupItem : BindableBase
    {
        private string _text;
        private string _sound;
        private string _image = "image_placeholder.png";

        public string Text
        {
            get => _text;
            set => SetProperty(ref _text, value);
        }

        public string Sound
        {
            get => _sound;
            set => SetProperty(ref _sound, value);
        }

        public string Image
        {
            get => _image;
            set => SetProperty(ref _image, value);
        }
    }
}