﻿using Prism.Mvvm;

namespace MedX.App.Models
{
    public class Answer : BindableBase
    {
        private string _text;
        private bool _allowFreeText;
        private string _freeText;
        private object _value;
        private bool _isSelected;

        public string Text
        {
            get => _text;
            set => SetProperty(ref _text, value);
        }
        public bool AllowFreeText
        {
            get => _allowFreeText;
            set => SetProperty(ref _allowFreeText, value);
        }
        public string FreeText
        {
            get => _freeText;
            set => SetProperty(ref _freeText, value);
        }
        public object Value
        {
            get => _value;
            set => SetProperty(ref _value, value);
        }
        public bool IsSelected
        {
            get => _isSelected;
            set => SetProperty(ref _isSelected, value);
        }

    }
}