﻿using Prism.Mvvm;
using Xamarin.Forms;

namespace MedX.App.Models
{
    public class FlowListItem : BindableBase
    {
        private object _value;
        private Color _backgroundColor = Color.Transparent;
        public object Value
        {
            get => _value;
            set => SetProperty(ref _value, value);
        }
        public Color BackgroundColor
        {
            get => _backgroundColor;
            set => SetProperty(ref _backgroundColor, value);
        }

    }
}