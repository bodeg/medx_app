﻿using Prism.Mvvm;

namespace MedX.App.Models
{
    //1) Shan sent an email a few weeks ago indicating that the final design will be based only upon the GEN3 machine.
    //2) The GEN3 machine does not use resistance bands.Piston are used instead. Thus the term resistance needs to be replaced as follows:
    //   -Piston Color (options are Red, Blue, Green). 
    //    From a coding perspective, females start with the RED piston. 
    //    Males start with the blue piston.Green piston is used if patient needs greater challenge.
    //   -Piston Setting:  options are 1.0, 2.0, 3.0, 4.0
    //3) The GEN3 machine has two settings for range of motion.Thus, remove ROM in the application in degrees.Replace with:
    //   Flexion Setting:  options are 1-12
    //   Extension Limiter Setting: options are 1-9
    //4) The generation 3 workout cards are attached to help build out the patient daily goals.

    public enum PistonColor
    {
        Green = 1,
        Blue = 2,
        Red = 3,
    }

    public class Goal : BindableBase
    {
        private int _extensionLimiter;
        private int _flexionPosition;
        private PistonColor? _pistonColor;
        private int _pistonSettings = 1;
        private int _repetitions;

        public int Id { get; set; }

        public int ExtensionLimiter
        {
            get => _extensionLimiter;
            set => SetProperty(ref _extensionLimiter, value);
        }

        public int FlexionPosition
        {
            get => _flexionPosition;
            set => SetProperty(ref _flexionPosition, value);
        }

        public PistonColor? PistonColor
        {
            get => _pistonColor;
            set => SetProperty(ref _pistonColor, value);
        }

        public int PistonSettings
        {
            get => _pistonSettings;
            set => SetProperty(ref _pistonSettings, value);
        }

        public int Repetitions
        {
            get => _repetitions;
            set => SetProperty(ref _repetitions, value);
        }

        public Goal Clone()
        {
            // http://www.c-sharpcorner.com/article/cloning-objects-in-net-framework/
            return (Goal) MemberwiseClone();
        }

        public bool IsEqual(Goal goal)
        {
            return ExtensionLimiter == goal.ExtensionLimiter
                   && FlexionPosition == goal.FlexionPosition
                   && PistonColor == goal.PistonColor
                   && PistonSettings == goal.PistonSettings
                   && Repetitions == goal.Repetitions;
        }
    }
}