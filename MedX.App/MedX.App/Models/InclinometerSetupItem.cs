﻿using Prism.Mvvm;

namespace MedX.App.Models
{
    public enum RangeOfMotion
    {
        Flexion,
        ExtLimiter
    }

    public class InclinometerSetupItem : BindableBase
    {
        private RangeOfMotion _rangeOfMotion;
        private int _romSettings;
        private double? _value;

        public RangeOfMotion RangeOfMotion
        {
            get => _rangeOfMotion;
            set => SetProperty(ref _rangeOfMotion, value);
        }

        public int RomSettings
        {
            get => _romSettings;
            set => SetProperty(ref _romSettings, value);
        }

        public double? Value
        {
            get => _value;
            set => SetProperty(ref _value, value);
        }
    }
}