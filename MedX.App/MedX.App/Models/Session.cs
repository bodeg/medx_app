﻿using System;
using System.ComponentModel;
using Prism.Mvvm;

namespace MedX.App.Models
{
    public enum SessionStatus
    {
        NotStarted,
        Completed,
        Incomplete,
        Passed
    }

    public enum SessionType
    {
        Flex,
        Endurance
    }

    public class Session : BindableBase
    {
        private DateTime _time;
        private SessionType? _type;
        private SessionStatus? _status;

        public int Id { get; set; }
        public DateTime Time
        {
            get => _time;
            set => SetProperty(ref _time, value);
        }

        public SessionType? Type
        {
            get => _type;
            set => SetProperty(ref _type, value);
        }

        public SessionStatus? Status
        {
            get => _status;
            set => SetProperty(ref _status, value);
        }

        public Goal CurrGoal { get; set; }
        public Goal PrevGoal { get; set; }
        public Goal CustomGoal { get; set; }
        public Goal ActualGoal { get; set; }
        public Answer ReasonToReduceGoal { get; set; }

        protected override void OnPropertyChanged(PropertyChangedEventArgs args)
        {
            base.OnPropertyChanged(args);

            if (args.PropertyName.Equals(nameof(Time)))
            {
                UpdateStatus();
            }
        }

        public void UpdateStatus()
        {
            if (Status != SessionStatus.Completed)
            {
                Status = Time >= DateTime.Now ? SessionStatus.NotStarted : SessionStatus.Passed;
            }
        }

        public void ResetValues()
        {
            CustomGoal = CurrGoal.Clone();
            ActualGoal = CurrGoal.Clone();
        }
    }
}

/*
var connectionString = App.FileHelper.GetLocalFilePath(@"MyData.db");
            using (var db = new LiteDatabase(connectionString))
            {
                var goals = db.GetCollection<SessionGoal>("goals");

                // Insert new customer document (Id will be auto-incremented)
                Goal.Id = 0;
                goals.Insert(Goal);

                var results = goals.FindAll();
                foreach (var goal in results)
                {
                    Debug.WriteLine($"{goal.Id} - {goal.FlexionPosition}");
                }
            }
*/