﻿using Xamarin.Forms;

namespace MedX.App.ViewCells
{
    public class AnswerDataTemplate : Xamarin.Forms.DataTemplateSelector
    {
        private readonly DataTemplate _deviceDataTemplate;

        public AnswerDataTemplate()
        {
            // Retain instances!
            _deviceDataTemplate = new DataTemplate(typeof(AnswerViewCell));
        }

        protected override DataTemplate OnSelectTemplate(object item, BindableObject container)
        {
            return _deviceDataTemplate;
        }

    }
}
