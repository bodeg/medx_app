﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MedX.App.ViewCells
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class AnswerViewCell : ViewCell
	{
		public AnswerViewCell ()
		{
			InitializeComponent ();
		}
	}
}