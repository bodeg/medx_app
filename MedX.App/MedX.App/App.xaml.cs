﻿using System;
using System.Globalization;
using System.Reflection;
using DLToolkit.Forms.Controls;
using MedX.App.Helpers;
using Prism.DryIoc;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Prism;
using Prism.Ioc;
using Prism.Mvvm;
using Prism.Plugin.Popups;
using MedX.App.Views;
using MedX.App.Views.Ble;
using MedX.App.Views.Demo;
using MedX.App.Views.Session;
using BaseBlePage = MedX.App.Views.Ble.BaseBlePage;
using BleInfoPage = MedX.App.Views.Ble.BleInfoPage;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace MedX.App
{
    public partial class App : PrismApplication
    {
        /* 
         * The Xamarin Forms XAML Previewer in Visual Studio uses System.Activator.CreateInstance.
         * This imposes a limitation in which the App class must have a default constructor. 
         * App(IPlatformInitializer initializer = null) cannot be handled by the Activator.
         */
        public App() : this(null)
        {
        }

        public App(IPlatformInitializer initializer) : base(initializer)
        {
        }

        //private static ILogger _logger;
        //public static ILogger Logger => _logger ?? (_logger = DependencyService.Get<ILogger>());

        private static IFileHelper _fileHelper;
        public static IFileHelper FileHelper => _fileHelper ?? (_fileHelper = DependencyService.Get<IFileHelper>());

        protected override async void OnInitialized()
        {
            InitializeComponent();

            // https://github.com/daniel-luberda/DLToolkit.Forms.Controls/tree/master/FlowListView
            FlowListView.Init();

            // http://brianlagunas.com/getting-started-prisms-new-viewmodellocator/
            // Change those Nasty Conventions
            ViewModelLocationProvider.SetDefaultViewTypeToViewModelTypeResolver((viewType) =>
            {
                if (viewType == typeof(NavigationPage))
                    return null;

                var viewName = viewType.FullName;
                viewName = viewName.Replace(".Views.", ".ViewModels.");
                var viewAssemblyName = viewType.GetTypeInfo().Assembly.FullName;
                var suffix = viewName.EndsWith("View") ? "Model" : "ViewModel";
                var viewModelName = string.Format(CultureInfo.InvariantCulture, "{0}{1}, {2}", 
                    viewName, suffix, viewAssemblyName);

                var type = Type.GetType(viewModelName);

                if (type == null)
                {
                    const string page = "Page";
                    if (viewName.EndsWith(page))
                    {
                        viewModelName = string.Format(CultureInfo.InvariantCulture, "{0}{1}, {2}",
                            viewName.Substring(0, viewName.Length - page.Length), suffix, viewAssemblyName);

                        type = Type.GetType(viewModelName);
                    }
                }

                return type;
            });

            //var pageName = MainPage;
            //var pageName = nameof(DemoPage);

            //var pageName = Navigation.General.Login;
            var pageName = Navigation.General.Home;

            //var pageName = Navigation.Session.PainLevelSurvey;
            //var pageName = Navigation.Session.CurrGoal;            
            //var pageName = Navigation.Session.ReasonToReduce;
            //var pageName = Navigation.Session.PrevGoal;
            //var pageName = Navigation.Session.CustomGoal;
            //var pageName = Navigation.Session.MachineSetUp;   
            //var pageName = Navigation.Session.Workout;
            //var pageName = Navigation.Session.WorkoutSummary;
            //var pageName = Navigation.Session.Scores;
            //var pageName = Navigation.Session.MachineSetUpConfig;

            //var pageName = Navigation.Ble.BleInfo;
            //var pageName = Navigation.Ble.BleScan;
            //var pageName = Navigation.Ble.Calibration;

            await NavigationService.NavigateAsync($"NavigationPage/{pageName}");
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.RegisterPopupNavigationService();
            //containerRegistry.RegisterInstance(CreateLogger());

            //containerRegistry.RegisterForNavigation<DemoBasePage>();
            containerRegistry.RegisterForNavigation<DemoPage>();
            containerRegistry.RegisterForNavigation<DemoPopupView>();

            containerRegistry.RegisterForNavigation<NavigationPage>();
            containerRegistry.RegisterForNavigation<MainPage>();           

            containerRegistry.RegisterForNavigation<LoginPage>();
            containerRegistry.RegisterForNavigation<HomePage>();
            containerRegistry.RegisterForNavigation<EntryPopupView>();

            //containerRegistry.RegisterForNavigation<BasePage>();
            //containerRegistry.RegisterForNavigation<BaseSessionPage>();   
            //containerRegistry.RegisterForNavigation<BaseGoalPage>();
            containerRegistry.RegisterForNavigation<CurrGoalPage>();
            containerRegistry.RegisterForNavigation<ReasonToReducePage>();
            containerRegistry.RegisterForNavigation<PrevGoalPage>();
            containerRegistry.RegisterForNavigation<CustomGoalPage>();
            containerRegistry.RegisterForNavigation<MachineSetUpPage>();
            containerRegistry.RegisterForNavigation<PainLevelSurveyPage>();
            containerRegistry.RegisterForNavigation<WorkoutPage>();
            containerRegistry.RegisterForNavigation<WorkoutSummaryPage>();
            containerRegistry.RegisterForNavigation<ScoresPage>();
            containerRegistry.RegisterForNavigation<BleScanPage>();
            containerRegistry.RegisterForNavigation<MachineSetUpConfigPage>();
            containerRegistry.RegisterForNavigation<CalibrationPage>();
            containerRegistry.RegisterForNavigation<BaseBlePage>();
            containerRegistry.RegisterForNavigation<BleInfoPage>();
        }

        //protected override void RegisterTypes()
        //{
        //    Container.RegisterTypeForNavigation<NavigationPage>();
        //    Container.RegisterTypeForNavigation<MainPage>();

        //    Container.RegisterTypeForNavigation<LoginPage>();
        //    Container.RegisterTypeForNavigation<HomePage>();
        //    //Container.RegisterTypeForNavigation<DemoBasePage>();
        //    Container.RegisterTypeForNavigation<DemoPage>();

        //    //Container.RegisterTypeForNavigation<BaseSessionPage>();
        //    Container.RegisterTypeForNavigation<BaseGoalPage>();
        //    Container.RegisterTypeForNavigation<SessionCustomizePage>();
        //    Container.RegisterTypeForNavigation<ReasonToReducePage>();
        //}
    }
}

