﻿using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Syncfusion.ListView.XForms;


namespace MedX.App.Behaviors
{
    /*


    [Preserve(AllMembers = true)]
    public class SfListViewSwipingBehavior : Behavior<Syncfusion.ListView.XForms.SfListView>
    {
        #region Fields

        private ListViewSwipingViewModel SwipingViewModel;
        private Syncfusion.ListView.XForms.SfListView ListView;

        #endregion

        #region Overrides
        protected override void OnAttachedTo(Syncfusion.ListView.XForms.SfListView bindable)
        {
            ListView = bindable;
            ListView.SwipeStarted += ListView_SwipeStarted;
            ListView.SwipeEnded += ListView_SwipeEnded;

            SwipingViewModel = new ListViewSwipingViewModel();
            SwipingViewModel.sfListView = ListView;
            bindable.BindingContext = SwipingViewModel;
            ListView.ItemsSource = SwipingViewModel.InboxInfo;
            base.OnAttachedTo(bindable);
        }
        protected override void OnDetachingFrom(Syncfusion.ListView.XForms.SfListView bindable)
        {
            ListView.SwipeStarted -= ListView_SwipeStarted;
            ListView.SwipeEnded -= ListView_SwipeEnded;
            ListView = null;
            SwipingViewModel = null;
            base.OnDetachingFrom(bindable);
        }
        #endregion

        #region Events
        private void ListView_SwipeEnded(object sender, SwipeEndedEventArgs e)
        {
            SwipingViewModel.ItemIndex = e.ItemIndex;
        }

        private void ListView_SwipeStarted(object sender, SwipeStartedEventArgs e)
        {
            SwipingViewModel.ItemIndex = -1;
        }
        #endregion
    }


    */
}