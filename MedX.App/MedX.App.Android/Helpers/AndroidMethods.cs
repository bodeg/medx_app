﻿using MedX.App.Droid.Helpers;
using MedX.App.Helpers;

[assembly: Xamarin.Forms.Dependency(typeof(AndroidMethods))]
namespace MedX.App.Droid.Helpers
{
    public class AndroidMethods : IAndroidMethods
    {
        public void CloseApp()
        {
            Android.OS.Process.KillProcess(Android.OS.Process.MyPid());
        }
    }
}
