﻿using System;
using System.IO;
using MedX.App.Droid.Helpers;
using MedX.App.Helpers;
using Xamarin.Forms;

[assembly: Dependency(typeof(FileHelper))]

namespace MedX.App.Droid.Helpers
{
    public class FileHelper : IFileHelper
    {
        public string GetLocalFilePath(string filename)
        {
            //var path = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            //return Path.Combine(path, filename);

            var dir = Path.Combine(Android.OS.Environment.ExternalStorageDirectory.ToString(), "MedX");
            return Path.Combine(Directory.Exists(dir) ? dir : Directory.CreateDirectory(dir).FullName, filename);
        }
    }
}