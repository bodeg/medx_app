﻿using System;
using System.IO;
using System.Threading.Tasks;
using MedX.App.Helpers;
using Xamarin.Forms;

namespace MedX.App.Droid.Helpers
{
    public static class ExceptionLogger
    {
        public static void Init()
        {
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
            TaskScheduler.UnobservedTaskException += TaskScheduler_UnobservedTaskException;
        }

        private static void TaskScheduler_UnobservedTaskException(object sender, UnobservedTaskExceptionEventArgs unobservedTaskExceptionEventArgs)
        {
            var newExc = new Exception("TaskScheduler_UnobservedTaskException", unobservedTaskExceptionEventArgs.Exception);
            LogUnhandledException(newExc);
        }

        private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs unhandledExceptionEventArgs)
        {
            var newExc = new Exception("CurrentDomain_UnhandledException", unhandledExceptionEventArgs.ExceptionObject as Exception);
            LogUnhandledException(newExc);
        }

        internal static void LogUnhandledException(Exception exception)
        {
            try
            {
                var errorFilePath = DependencyService.Get<IFileHelper>().GetLocalFilePath("exceptions.txt");

                var errorMessage = $"Time: {DateTime.Now}\r\nError: Unhandled Exception\r\n{exception}";
                File.AppendAllText(errorFilePath, errorMessage);
            }
            catch
            {
                // ignored
            }
        }
    }
}