﻿using System;
using System.Diagnostics;
using Android.Runtime;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using Button = Android.Widget.Button;
using RoutingEffects = MedX.App.Effects;
using PlatformEffects = MedX.App.Droid.Effects;

//[assembly: ResolutionGroupName("MyCompany")]
//[assembly: ExportEffect(typeof(ButtonNotAllCapsEffect), "ButtonNotAllCapsEffect")]

[assembly: ResolutionGroupName("MedX.App.Effects")]
[assembly: ExportEffect(
    effectType: typeof(PlatformEffects.ButtonNotAllCapsEffect),
    uniqueName: nameof(RoutingEffects.ButtonNotAllCapsEffect))]
namespace MedX.App.Droid.Effects
{
    [Preserve(AllMembers = true)]
    public class ButtonNotAllCapsEffect : PlatformEffect
    {
        protected override void OnAttached()
        {
            try
            {
                if (Control is Button control)
                {
                    control.SetAllCaps(false);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"[{GetType().FullName}]: {ex.Message}");
            }
        }

        protected override void OnDetached()
        {
        }

        protected override void OnElementPropertyChanged(System.ComponentModel.PropertyChangedEventArgs args)
        {
            base.OnElementPropertyChanged(args);
        }
    }
}
