﻿using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Views;
using ImageCircle.Forms.Plugin.Droid;
using MedX.App.Droid.Helpers;
using Plugin.Toasts;
using Prism;
using Prism.Ioc;

namespace MedX.App.Droid
{
    [Activity(Label = "MedX.App", 
        Icon = "@drawable/icon",
        //Theme = "@style/MainTheme", 
        Theme = "@style/splashscreen",
        MainLauncher = true, 
        //ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation,
        ScreenOrientation = ScreenOrientation.Portrait)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            ExceptionLogger.Init();

            // https://xamarinhelp.com/creating-splash-screen-xamarin-forms/
            base.Window.RequestFeature(WindowFeatures.ActionBar);
            // Name of the MainActivity theme you had there before.
            // Or you can use global::Android.Resource.Style.ThemeHoloLight
            base.SetTheme(Resource.Style.MainTheme);

            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(bundle);

            global::Rg.Plugins.Popup.Popup.Init(this, bundle);

            global::Xamarin.Forms.Forms.Init(this, bundle);

            // https://xamarinhelp.com/toast-notifications-xamarin-forms/
            Xamarin.Forms.DependencyService.Register<ToastNotification>();
            ToastNotification.Init(this, new PlatformOptions() { SmallIconDrawable = Android.Resource.Drawable.IcDialogInfo });

            // https://github.com/jamesmontemagno/ImageCirclePlugin
            ImageCircleRenderer.Init();

            // https://github.com/luberda-molinet/FFImageLoading/wiki/Xamarin.Forms-API
            FFImageLoading.Forms.Droid.CachedImageRenderer.Init(enableFastRenderer: true);

            LoadApplication(new App(new AndroidInitializer()));
        }
    }

    public class AndroidInitializer : IPlatformInitializer
    {
        //public void RegisterTypes(IContainer container)
        //{
        //    // Register any platform specific implementations
        //}

        public void RegisterTypes(IContainerRegistry containerRegistry)
        {
            // Register any platform specific implementations
        }
    }
}

