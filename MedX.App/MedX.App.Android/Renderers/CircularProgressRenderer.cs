﻿using System.ComponentModel;
using Android.Content;
using com.refractored.monodroidtoolkit;
using MedX.App.CustomControls;
using MedX.App.Droid.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(CircularProgress), typeof(CircularProgressRenderer))]
namespace MedX.App.Droid.Renderers
{
    // What is a HoloCircularProgressBar
    // https://github.com/jamesmontemagno/MonoDroidToolkit/wiki/HoloCircularProgressBar
    public class CircularProgressRenderer : ViewRenderer<CircularProgress, HoloCircularProgressBar>
    {
        public CircularProgressRenderer(Context context) : base(context)
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<CircularProgress> e)
        {
            base.OnElementChanged(e);

            if (e.OldElement != null || this.Element == null)
                return;

            //< com.refractored.monodroidtoolkit.HoloCircularProgressBar
            //    android: id = "@+id/holoCircularProgressBar1"
            //    android: layout_height = "wrap_content"
            //    android: layout_width = "wrap_content"
            //    app: circular_stroke_width = "10dp"
            //    app: circular_progress = "40"
            //    app: circular_marker_progress = "60"
            //    app: circular_progress_color = "@android:color/holo_orange_dark"
            //    app: circular_progress_background_color = "#cccccc"
            //    app: circular_gravity = "center"
            //    app: circular_indeterminate = "false"
            //    app: circular_indeterminate_interval = "50" />

             //var context = Forms.Context;
             var context = Android.App.Application.Context;
            var progress = new HoloCircularProgressBar(context)
            {
                Max = Element.Max,
                Progress = Element.Progress,
                Indeterminate = Element.Indeterminate,
                ProgressColor = Element.ProgressColor.ToAndroid(),
                ProgressBackgroundColor = Element.ProgressBackgroundColor.ToAndroid(),
                IndeterminateInterval = Element.IndeterminateSpeed,
                CircleStrokeWidth = Element.CircleStrokeWidth
            };

            SetNativeControl(progress);
        }


        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            if (Control == null || Element == null)
                return;

            if (e.PropertyName == CircularProgress.MaxProperty.PropertyName)
            {
                Control.Max = Element.Max;
            }
            else if (e.PropertyName == CircularProgress.ProgressProperty.PropertyName)
            {
                Control.Progress = Element.Progress;
            }
            else if (e.PropertyName == CircularProgress.IndeterminateProperty.PropertyName)
            {
                Control.Indeterminate = Element.Indeterminate;
            }
            else if (e.PropertyName == CircularProgress.ProgressBackgroundColorProperty.PropertyName)
            {
                Control.ProgressBackgroundColor = Element.ProgressBackgroundColor.ToAndroid();
            }
            else if (e.PropertyName == CircularProgress.ProgressColorProperty.PropertyName)
            {
                Control.ProgressColor = Element.ProgressColor.ToAndroid();
            }
            else if (e.PropertyName == CircularProgress.IndeterminateSpeedProperty.PropertyName)
            {
                Control.IndeterminateInterval = Element.IndeterminateSpeed;
            }
            else if (e.PropertyName == CircularProgress.CircleStrokeWidthProperty.PropertyName)
            {
                Control.CircleStrokeWidth = Element.CircleStrokeWidth;
            }

        }
    }
}